FROM alpine:3.11

COPY ./prwizard /usr/local/bin
COPY ./ticketmaster /usr/local/bin
COPY ./watcher /usr/local/bin

RUN apk add --no-cache ca-certificates curl
