package main

import (
	"flag"

	"github.com/leominov/prwizard/pkg/logrusutil"
	"github.com/leominov/prwizard/pkg/pullreminders"
	"github.com/sirupsen/logrus"
)

var (
	dryRun   = flag.Bool("dry-run", false, "Dry run for testing")
	logLevel = flag.String("log-level", "info", "Logging level")
)

func main() {
	flag.Parse()

	logrus.SetFormatter(logrusutil.NewDefaultFieldsFormatter(nil, logrus.Fields{"component": "pullreminders"}))
	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	config := pullreminders.ConfigFromEnv()
	config.DryRun = *dryRun
	pr, err := pullreminders.New(logrus.WithField("component", "pullreminders"), config)
	if err != nil {
		logrus.Fatal(err)
	}

	err = pr.Run()
	if err != nil {
		logrus.Fatal(err)
	}
}
