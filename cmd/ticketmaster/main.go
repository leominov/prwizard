package main

import (
	"bytes"
	"flag"
	"io/ioutil"
	"net/http"
	_ "net/http/pprof"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/gorilla/mux"
	"github.com/leominov/prwizard/pkg/jira"
	"github.com/leominov/prwizard/pkg/logrusutil"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/leominov/prwizard/pkg/ticketmaster"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

type options struct {
	port     int
	logLevel string

	jiraEndpoint  string
	stashEndpoint string
	jiraAuthFile  string
	stashAuthFile string
}

func gatherOptions() options {
	o := options{}
	flag.IntVar(&o.port, "port", 8890, "Port to listen on.")
	flag.StringVar(&o.logLevel, "log-level", "info", "Logging level.")
	flag.StringVar(&o.jiraEndpoint, "jira-endpoint", "http://127.0.0.1:7990/", "Jira's API endpoint.")
	flag.StringVar(&o.jiraAuthFile, "jira-auth-file", "/etc/jira/auth", "Path to the file containing the Jira Authorization to use.")
	flag.StringVar(&o.stashEndpoint, "stash-endpoint", "http://127.0.0.1:7990/", "Stash's API endpoint.")
	flag.StringVar(&o.stashAuthFile, "stash-auth-file", "/etc/stash/auth", "Path to the file containing the Stash Authorization to use.")
	flag.Parse()
	return o
}

func main() {
	o := gatherOptions()
	logrus.SetFormatter(logrusutil.NewDefaultFieldsFormatter(nil, logrus.Fields{"component": "ticketmaster"}))
	if level, err := logrus.ParseLevel(o.logLevel); err == nil {
		logrus.SetLevel(level)
	}

	logrus.Info("Starting...")

	signal.Ignore(syscall.SIGTERM)

	jiraAuthSecretRaw, err := ioutil.ReadFile(o.jiraAuthFile)
	if err != nil {
		logrus.WithError(err).Fatal("Could not read Jira auth secret file.")
	}
	jiraAuthSecret := string(bytes.TrimSpace(jiraAuthSecretRaw))
	jiraClient := jira.NewClient(o.jiraEndpoint, nil)
	jiraClient.SetCredentials(jiraAuthSecret)

	stashAuthSecretRaw, err := ioutil.ReadFile(o.stashAuthFile)
	if err != nil {
		logrus.WithError(err).Fatal("Could not read Stash auth secret file.")
	}
	stashAuthSecret := string(bytes.TrimSpace(stashAuthSecretRaw))
	stashClient := stash.NewClient(o.stashEndpoint, nil)
	stashClient.SetCredentials(stashAuthSecret)

	botName, err := stashClient.BotName()
	if err == nil {
		logrus.Infof("Bot name: %s", botName)
	}

	server := &ticketmaster.Server{
		JiraClient:  jiraClient,
		StashClient: stashClient,
	}

	r := mux.NewRouter()
	r.NotFoundHandler = http.HandlerFunc(server.NotFoundHandler)
	r.HandleFunc("/issues/badges/{label}.svg", server.UnresolvedIssuesByLabelSVGHandler)
	r.HandleFunc("/issues/badges/status/{key}.svg", server.IssueStatusSVGHandler)
	r.HandleFunc("/tags/badges/{projectKey}/{repoSlug}.svg", server.LatestTagSVGHandler)
	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/", r)

	logrus.Fatal(http.ListenAndServe(":"+strconv.Itoa(o.port), nil))
}
