package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	_ "net/http/pprof"
	"net/url"
	"strconv"

	"github.com/leominov/prwizard/pkg/logrusutil"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/leominov/prwizard/pkg/watcher"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

type options struct {
	port     int
	logLevel string

	projectKey    string
	repoSlug      string
	stashEndpoint string
	stashAuthFile string
}

func (o *options) Validate() error {
	_, err := url.Parse(o.stashEndpoint)
	if err != nil {
		return fmt.Errorf("incorrect stash endpoint: %v", err)
	}
	return nil
}

func gatherOptions() options {
	o := options{}
	flag.IntVar(&o.port, "port", 8880, "Port to listen on.")
	flag.StringVar(&o.logLevel, "log-level", "info", "Logging level.")
	flag.StringVar(&o.repoSlug, "repo", "watcher-config", "Name of the repository with configuration.")
	flag.StringVar(&o.projectKey, "project", "devops", "Name of the repository's project with configuration.")
	flag.StringVar(&o.stashEndpoint, "stash-endpoint", "http://127.0.0.1:7990/", "Stash's API endpoint.")
	flag.StringVar(&o.stashAuthFile, "stash-auth-file", "/etc/stash/auth", "Path to the file containing the Stash Authorization to use.")
	flag.Parse()
	return o
}

func main() {
	o := gatherOptions()
	if err := o.Validate(); err != nil {
		logrus.Fatalf("Invalid options: %v", err)
	}
	logrus.SetFormatter(logrusutil.NewDefaultFieldsFormatter(nil, logrus.Fields{"component": "watcher"}))
	if level, err := logrus.ParseLevel(o.logLevel); err == nil {
		logrus.SetLevel(level)
	}

	authSecretRaw, err := ioutil.ReadFile(o.stashAuthFile)
	if err != nil {
		logrus.WithError(err).Fatal("Could not read auth secret file.")
	}
	authSecret := string(bytes.TrimSpace(authSecretRaw))

	stashClient := stash.NewClient(o.stashEndpoint, nil)
	stashClient.SetCredentials(authSecret)
	_, err = stashClient.BotName()
	if err != nil {
		logrus.WithError(err).Fatal("Error getting bot name.")
	}

	server := &watcher.Server{
		ProjectKey:  o.projectKey,
		RepoSlug:    o.repoSlug,
		StashClient: stashClient,
	}

	go func() {
		err := server.Run()
		if err != nil {
			logrus.Fatal(err)
		}
	}()

	http.Handle("/metrics", promhttp.Handler())
	logrus.Fatal(http.ListenAndServe(":"+strconv.Itoa(o.port), nil))
}
