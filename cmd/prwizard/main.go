package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	_ "net/http/pprof"
	"net/url"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/leominov/prwizard/pkg/hook"
	"github.com/leominov/prwizard/pkg/logrusutil"
	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/repoowners"
	"github.com/leominov/prwizard/pkg/slack"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

type options struct {
	port     int
	logLevel string
	dryRun   bool

	stashEndpoint     string
	stashAuthFile     string
	webhookSecretFile string
	slackTokenFile    string
}

func (o *options) Validate() error {
	_, err := url.Parse(o.stashEndpoint)
	if err != nil {
		return fmt.Errorf("incorrect stash endpoint: %v", err)
	}
	return nil
}

func gatherOptions() options {
	o := options{}
	flag.IntVar(&o.port, "port", 8888, "Port to listen on.")
	flag.BoolVar(&o.dryRun, "dry-run", true, "Dry run for testing.")
	flag.StringVar(&o.logLevel, "log-level", "info", "Logging level.")
	flag.StringVar(&o.stashEndpoint, "stash-endpoint", "http://127.0.0.1:7990/", "Stash's API endpoint.")
	flag.StringVar(&o.stashAuthFile, "stash-auth-file", "/etc/stash/auth", "Path to the file containing the Stash Authorization to use.")
	flag.StringVar(&o.webhookSecretFile, "hmac-secret-file", "/etc/webhook/hmac", "Path to the file containing the Bitbucket Server HMAC secret.")
	flag.StringVar(&o.slackTokenFile, "slack-token-file", "", "Path to the file containing the Slack token to use.")
	flag.Parse()
	return o
}

func main() {
	o := gatherOptions()
	if err := o.Validate(); err != nil {
		logrus.Fatalf("Invalid options: %v", err)
	}
	logrus.SetFormatter(logrusutil.NewDefaultFieldsFormatter(nil, logrus.Fields{"component": "prwizard"}))
	if level, err := logrus.ParseLevel(o.logLevel); err == nil {
		logrus.SetLevel(level)
	}

	logrus.Info("Starting...")

	signal.Ignore(syscall.SIGTERM)

	webhookSecretRaw, err := ioutil.ReadFile(o.webhookSecretFile)
	if err != nil {
		logrus.WithError(err).Fatal("Could not read webhook secret file.")
	}
	webhookSecret := bytes.TrimSpace(webhookSecretRaw)

	authSecretRaw, err := ioutil.ReadFile(o.stashAuthFile)
	if err != nil {
		logrus.WithError(err).Fatal("Could not read auth secret file.")
	}
	authSecret := string(bytes.TrimSpace(authSecretRaw))

	promMetrics := hook.NewMetrics()

	stashClient := stash.NewClient(o.stashEndpoint, nil)
	stashClient.SetCredentials(authSecret)
	botName, err := stashClient.BotName()
	if err != nil {
		logrus.WithError(err).Fatal("Error getting bot name.")
	}

	logrus.Infof("Bot name: %s", botName)

	var teamToken string
	if o.slackTokenFile != "" {
		teamTokenRaw, err := ioutil.ReadFile(o.slackTokenFile)
		if err != nil {
			logrus.WithError(err).Fatal("Could not read slack token file.")
		}
		teamToken = string(bytes.TrimSpace(teamTokenRaw))
	}

	var slackClient *slack.Client
	if !o.dryRun && teamToken != "" {
		logrus.Info("Using real slack client.")
		slackClient = slack.NewClient().SetToken(teamToken)
	}
	if slackClient == nil {
		logrus.Info("Using fake slack client.")
		slackClient = slack.NewFakeClient()
	}

	ownersClient := repoowners.NewClient(stashClient)

	pluginAgent := &plugins.PluginAgent{}
	pluginAgent.PluginClient = plugins.PluginClient{
		StashClient:  stashClient,
		OwnersClient: ownersClient,
		SlackClient:  slackClient,
		Logger:       logrus.WithField("agent", "plugin"),
	}

	server := &hook.Server{
		HMACSecret: webhookSecret,
		Plugins:    pluginAgent,
		Metrics:    promMetrics,
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {})
	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/hook", server)

	logrus.Fatal(http.ListenAndServe(":"+strconv.Itoa(o.port), nil))
}
