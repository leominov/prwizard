package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	"github.com/ghodss/yaml"
	"github.com/gorilla/mux"
	"github.com/leominov/prwizard/pkg/grower"
	"github.com/leominov/prwizard/pkg/logrusutil"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/sirupsen/logrus"
)

type options struct {
	port          int
	configFile    string
	stashEndpoint string
	stashAuthFile string
}

func (o *options) Validate() error {
	_, err := url.Parse(o.stashEndpoint)
	if err != nil {
		return fmt.Errorf("incorrect stash endpoint: %v", err)
	}
	return nil
}

func gatherOptions() options {
	o := options{}
	flag.IntVar(&o.port, "port", 8889, "Port to listen on.")
	flag.StringVar(&o.stashEndpoint, "stash-endpoint", "http://127.0.0.1:7990/", "Stash's API endpoint.")
	flag.StringVar(&o.stashAuthFile, "stash-auth-file", "/etc/stash/auth", "Path to the file containing the Stash Authorization to use.")
	flag.StringVar(&o.configFile, "config-file", "", "Configuration file for grower")
	flag.Parse()
	return o
}

func main() {
	o := gatherOptions()
	if err := o.Validate(); err != nil {
		logrus.Fatalf("Invalid options: %v", err)
	}
	logrus.SetFormatter(logrusutil.NewDefaultFieldsFormatter(nil, logrus.Fields{"component": "grower"}))

	authSecretRaw, err := ioutil.ReadFile(o.stashAuthFile)
	if err != nil {
		logrus.WithError(err).Fatal("Could not read auth secret file.")
	}
	authSecret := string(bytes.TrimSpace(authSecretRaw))

	stashClient := stash.NewClient(o.stashEndpoint, nil)
	stashClient.SetCredentials(authSecret)
	_, err = stashClient.BotName()
	if err != nil {
		logrus.WithError(err).Fatal("Error getting bot name.")
	}

	config := &grower.Config{}
	configRaw, err := ioutil.ReadFile(o.configFile)
	if err != nil {
		logrus.WithError(err).Fatal("Could not read config file.")
	}
	err = yaml.Unmarshal(configRaw, &config)
	if err != nil {
		logrus.WithError(err).Fatal("Could not process config file.")
	}

	server := &grower.Server{
		Config:      config,
		StashClient: stashClient,
	}

	r := mux.NewRouter()
	s := r.PathPrefix("/api").Subrouter()
	s.HandleFunc("/projects", server.ProjectsHandler)
	s.HandleFunc("/projects/{projectKey}", server.ProjectHandler)
	s.HandleFunc("/projects/{projectKey}/webhooks", server.ProjectWebhooksHandler)
	s.HandleFunc("/projects/{projectKey}/repos", server.ProjectReposHandler)
	s.HandleFunc("/projects/{projectKey}/repos/{repoSlug}", server.ProjectRepoHandler)
	s.HandleFunc("/projects/{projectKey}/repos/{repoSlug}/settings/pull-requests", server.RepoPRSettingsHandler)
	s.HandleFunc("/projects/{projectKey}/repos/{repoSlug}/webhooks", server.RepoWebhooksHandler)
	s.HandleFunc("/projects/{projectKey}/repos/{repoSlug}/webhooks/{id}", server.WebhookHandler)
	s.HandleFunc("/projects/{projectKey}/repos/{repoSlug}/webhooks/{id}/enable", server.EnableWebhookHandler)
	s.HandleFunc("/projects/{projectKey}/repos/{repoSlug}/webhooks/{id}/disable", server.DisableWebhookHandler)
	s.HandleFunc("/projects/{projectKey}/repos/{repoSlug}/tags", server.RepoTagsHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("static/grower")))
	http.Handle("/", r)

	logrus.Fatal(http.ListenAndServe(":"+strconv.Itoa(o.port), nil))
}
