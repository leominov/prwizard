package assign

import (
	"regexp"
	"strings"

	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
)

const (
	pluginName = "assign"
)

var (
	CCRegexp = regexp.MustCompile(`(?mi)^/(un)?cc(( +@?[-/\w\.\"]+?)*)\s*$`)
)

func init() {
	plugins.RegisterPullRequestCommentHandler(pluginName, handlePullRequestCommentEvent)
}

func parseLogins(text string) []string {
	var parts []string
	for _, p := range strings.Split(text, " ") {
		t := strings.Trim(p, "@ ")
		if t == "" {
			continue
		}
		t = strings.Replace(t, "\"", "", -1)
		parts = append(parts, t)
	}
	return parts
}

func handlePullRequestCommentEvent(pc plugins.PluginClient, prc stash.PullRequestCommentEvent) error {
	matches := CCRegexp.FindAllStringSubmatch(prc.Comment.Text, -1)
	if matches == nil {
		return nil
	}
	users := make(map[string]bool)
	for _, re := range matches {
		add := re[1] != "un"
		if re[2] == "" {
			users[prc.Actor.Slug] = add
		} else {
			for _, login := range parseLogins(re[2]) {
				users[login] = add
			}
		}
	}
	var toAdd, toRemove []string
	for login, add := range users {
		if add {
			toAdd = append(toAdd, login)
		} else {
			toRemove = append(toRemove, login)
		}
	}
	if len(toRemove) > 0 {
		pc.Logger.Infof(
			"Removing from %s/%s#%d: %v",
			prc.PullRequest.ToRef.Repository.Project.Key,
			prc.PullRequest.ToRef.Repository.Slug,
			prc.PullRequest.ID,
			toRemove,
		)
		for _, user := range toRemove {
			err := pc.StashClient.DeleteReviewer(
				prc.PullRequest.ToRef.Repository.Project.Key,
				prc.PullRequest.ToRef.Repository.Slug,
				prc.PullRequest.ID,
				user,
			)
			if err != nil {
				pc.Logger.Error(err)
			}
		}
	}
	if len(toAdd) > 0 {
		pc.Logger.Infof(
			"Adding to %s/%s#%d: %v",
			prc.PullRequest.ToRef.Repository.Project.Key,
			prc.PullRequest.ToRef.Repository.Slug,
			prc.PullRequest.ID,
			toAdd,
		)
		for _, user := range toAdd {
			err := pc.StashClient.AddReviewer(
				prc.PullRequest.ToRef.Repository.Project.Key,
				prc.PullRequest.ToRef.Repository.Slug,
				prc.PullRequest.ID,
				user,
			)
			if err != nil {
				pc.Logger.Error(err)
			}
		}
	}
	return nil
}
