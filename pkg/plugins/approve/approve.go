package approve

import (
	"fmt"
	"strings"

	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
)

const (
	pluginName      = "approve"
	messageTemplate = `:black_joker: На основании содержимого файла OWNERS (%s), для pull-request #%d рекомендованы аппруверы: %s.`
)

func init() {
	plugins.RegisterPullRequestHandler(pluginName, handlePullRequestEvent)
}

func handlePullRequestEvent(pc plugins.PluginClient, pre stash.PullRequestEvent) error {
	botName, err := pc.StashClient.BotName()
	if err != nil {
		return err
	}
	if pre.Actor.Slug == botName {
		return nil
	}
	affectedFiles, err := pc.StashClient.GetAffectedFiles(
		pre.PullRequest.ToRef.Repository.Project.Key,
		pre.PullRequest.ToRef.Repository.Slug,
		pre.PullRequest.ID,
	)
	if err != nil {
		return err
	}
	owners, err := pc.OwnersClient.LoadRepoOwners(
		pre.PullRequest.ToRef.Repository.Project.Key,
		pre.PullRequest.ToRef.Repository.Slug,
		pre.PullRequest.ToRef.LatestCommit,
	)
	if err != nil {
		return err
	}
	ownersList := owners.FindOwnersForFiles(affectedFiles)
	var addedOwnersList []string
	for _, owner := range ownersList {
		if owner == pre.Actor.Slug {
			continue
		}
		if pre.PullRequest.IsUserInParticipants(owner) {
			continue
		}
		err := pc.StashClient.AddReviewer(
			pre.PullRequest.ToRef.Repository.Project.Key,
			pre.PullRequest.ToRef.Repository.Slug,
			pre.PullRequest.ID,
			owner,
		)
		if err != nil {
			pc.Logger.Error(err)
		}
		addedOwnersList = append(addedOwnersList, fmt.Sprintf(`@"%s"`, owner))
	}
	if pre.EventKey == "pr:opened" && len(addedOwnersList) > 0 {
		_, err = pc.StashClient.CreateComment(
			pre.PullRequest.ToRef.Repository.Project.Key,
			pre.PullRequest.ToRef.Repository.Slug,
			pre.PullRequest.ID,
			fmt.Sprintf(
				messageTemplate,
				pre.PullRequest.ToRef.DisplayID,
				pre.PullRequest.ID,
				strings.Join(addedOwnersList, ", "),
			),
		)
		return err
	}
	return nil
}
