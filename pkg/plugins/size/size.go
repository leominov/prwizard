package size

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
)

type size int

const (
	pluginName = "size"

	sizeXS size = iota
	sizeS
	sizeM
	sizeL
	sizeXL
	sizeXXL

	labelXS     = "size/XS"
	labelS      = "size/S"
	labelM      = "size/M"
	labelL      = "size/L"
	labelXL     = "size/XL"
	labelXXL    = "size/XXL"
	labelUnkown = "size/?"
)

var (
	labelPrefixRegExp = regexp.MustCompile(`\[(size\/\w+)\]`)
)

func init() {
	plugins.RegisterPullRequestHandler(pluginName, handlePullRequestEvent)
}

func getSizeLabel(stashCli *stash.Client, projectKey, repoSlug string, prID int) (string, error) {
	resp, err := stashCli.GetPullRequestDiff(projectKey, repoSlug, prID)
	if err != nil {
		return "", err
	}
	var lineCount int
	for _, diff := range resp.Diffs {
		for _, hank := range diff.Hunks {
			for _, segment := range hank.Segments {
				if segment.Type == stash.SegmentAdded || segment.Type == stash.SegmtntRemoved {
					lineCount += len(segment.Lines)
				}
			}
		}
	}
	size := bucket(lineCount)
	return size.label(), nil
}

func handlePullRequestEvent(pc plugins.PluginClient, pre stash.PullRequestEvent) error {
	botName, err := pc.StashClient.BotName()
	if err != nil {
		return err
	}
	if pre.Actor.Slug == botName {
		return nil
	}
	label, err := getSizeLabel(
		pc.StashClient,
		pre.PullRequest.ToRef.Repository.Project.Key,
		pre.PullRequest.ToRef.Repository.Slug,
		pre.PullRequest.ID,
	)
	if err != nil {
		return err
	}
	matches := labelPrefixRegExp.FindStringSubmatch(pre.PullRequest.Title)
	var title string
	if len(matches) >= 2 {
		currentLabel := matches[1]
		if label == currentLabel {
			return nil
		}
		title = labelPrefixRegExp.ReplaceAllString(pre.PullRequest.Title, "")
		title = strings.TrimSpace(title)
		title = fmt.Sprintf("%s [%s]", title, label)
	} else {
		title = fmt.Sprintf("%s [%s]", pre.PullRequest.Title, label)
	}
	pre.PullRequest.Title = title
	err = pc.StashClient.UpdatePullRequest(
		pre.PullRequest.ToRef.Repository.Project.Key,
		pre.PullRequest.ToRef.Repository.Slug,
		"title",
		pre.PullRequest,
	)
	if err != nil {
		return err
	}
	return nil
}

func (s size) label() string {
	switch s {
	case sizeXS:
		return labelXS
	case sizeS:
		return labelS
	case sizeM:
		return labelM
	case sizeL:
		return labelL
	case sizeXL:
		return labelXL
	case sizeXXL:
		return labelXXL
	}

	return labelUnkown
}

func bucket(lineCount int) size {
	if lineCount < 10 {
		return sizeXS
	} else if lineCount < 30 {
		return sizeS
	} else if lineCount < 100 {
		return sizeM
	} else if lineCount < 500 {
		return sizeL
	} else if lineCount < 1000 {
		return sizeXL
	}

	return sizeXXL
}
