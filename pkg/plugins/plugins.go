package plugins

import (
	"sync"

	"github.com/leominov/prwizard/pkg/repoowners"
	"github.com/leominov/prwizard/pkg/slack"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/sirupsen/logrus"
)

var (
	pullRequestHandlers        = map[string]PullRequestHandler{}
	pullRequestCommentHandlers = map[string]PullRequestCommentHandler{}
)

type (
	PullRequestHandler        func(PluginClient, stash.PullRequestEvent) error
	PullRequestCommentHandler func(PluginClient, stash.PullRequestCommentEvent) error
)

type PluginClient struct {
	StashClient  *stash.Client
	SlackClient  *slack.Client
	OwnersClient *repoowners.Client

	PluginConfig *Configuration

	Logger *logrus.Entry
}

type Configuration struct {
	Approve []string `json:"approve,omitempty"`
	Slack   Slack    `json:"slack,omitempty"`
}

type Slack struct {
	MentionChannels []string `json:"mentionchannels,omitempty"`
}

type PluginAgent struct {
	PluginClient

	mut           sync.Mutex
	configuration *Configuration
}

func RegisterPullRequestCommentHandler(name string, fn PullRequestCommentHandler) {
	pullRequestCommentHandlers[name] = fn
}

func RegisterPullRequestHandler(name string, fn PullRequestHandler) {
	pullRequestHandlers[name] = fn
}

func (pa *PluginAgent) Config() *Configuration {
	pa.mut.Lock()
	defer pa.mut.Unlock()
	return pa.configuration
}

func (pa *PluginAgent) PullRequestHandlers() map[string]PullRequestHandler {
	pa.mut.Lock()
	defer pa.mut.Unlock()
	return pullRequestHandlers
}

func (pa *PluginAgent) PullRequestCommentHandlers() map[string]PullRequestCommentHandler {
	pa.mut.Lock()
	defer pa.mut.Unlock()
	return pullRequestCommentHandlers
}
