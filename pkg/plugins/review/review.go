package review

import (
	"fmt"

	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/leominov/prwizard/pkg/textutil"
)

const (
	pluginName      = "review"
	commandReview   = "/review"
	messageTemplate = "@%s ask you to review PR: %s"
)

func init() {
	plugins.RegisterPullRequestCommentHandler(pluginName, handlePullRequestCommentEvent)
}

func handlePullRequestCommentEvent(pc plugins.PluginClient, prc stash.PullRequestCommentEvent) error {
	botName, err := pc.StashClient.BotName()
	if err != nil {
		return err
	}
	if prc.Comment.Author.Slug == botName {
		return nil
	}
	if !textutil.TextHasCommand(prc.Comment.Text, commandReview) {
		return nil
	}
	if prc.Comment.Author.Slug != prc.PullRequest.Author.User.Slug {
		return nil
	}
	prLink := fmt.Sprintf("%s%s", pc.StashClient.Endpoint(), prc.PullRequest.Path())
	users := prc.PullRequest.GetAllParticipants()
	for _, user := range users {
		if user.Approved {
			continue
		}
		message := fmt.Sprintf(messageTemplate, prc.Comment.Author.Slug, prLink)
		pc.SlackClient.WriteMessage(message, fmt.Sprintf("@%s", user.User.Slug))
	}
	return nil
}
