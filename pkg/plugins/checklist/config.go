package checklist

import (
	"strings"

	"github.com/cloudfoundry/cli/util/glob"
	"gopkg.in/yaml.v2"

	"github.com/leominov/prwizard/pkg/textutil"
)

const (
	messageTemplate = `Проверьте пункты перед передачей PR на ревью.`
)

type Configuration struct {
	Message string   `yaml:"message"`
	Checks  []*Check `yaml:"checks"`
}

type Check struct {
	Name     string      `yaml:"name"`
	OnChange StringSlice `yaml:"on_change"`
}

type checkType Check

type StringSlice []string

func (c *Check) UnmarshalYAML(unmarshal func(interface{}) error) error {
	if err := unmarshal((*checkType)(c)); err == nil {
		return nil
	}
	var str string
	err := unmarshal(&str)
	if err != nil {
		return err
	}
	c.Name = str
	return nil
}

func (s *StringSlice) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var stringType string
	if err := unmarshal(&stringType); err == nil {
		*s = []string{stringType}
		return nil
	}

	var sliceType []string
	if err := unmarshal(&sliceType); err != nil {
		return err
	}
	*s = sliceType
	return nil
}

func Parse(body string) *Configuration {
	c := &Configuration{}

	if err := yaml.Unmarshal([]byte(body), c); err == nil {
		c.fillDefaults()
		return c
	}

	lines := textutil.SplitTextByLine(body)
	c.fillDefaults()
	for i, line := range lines {
		if !strings.HasPrefix(line, "* ") {
			if i == 0 {
				c.Message = strings.TrimSpace(line)
			}
			continue
		}
		line = strings.TrimPrefix(line, "* ")
		line = strings.TrimSpace(line)
		if len(line) > 0 {
			c.Checks = append(c.Checks, &Check{
				Name: line,
			})
		}
	}

	return c
}

func (c *Configuration) fillDefaults() {
	if len(c.Message) == 0 {
		c.Message = messageTemplate
	}
}

func (c *Configuration) RequiredAffectedFiles() bool {
	for _, check := range c.Checks {
		if len(check.OnChange) > 0 {
			return true
		}
	}
	return false
}

func (c *Configuration) ChecksByAffectedFiles(files []string) []*Check {
	var checks []*Check
	for _, check := range c.Checks {
		if len(check.OnChange) == 0 {
			checks = append(checks, check)
			continue
		}
		for _, onChange := range check.OnChange {
			gl := glob.MustCompileGlob(onChange)
			for _, file := range files {
				if gl.Match(file) {
					checks = append(checks, check)
					break
				}
			}
		}
	}
	return checks
}
