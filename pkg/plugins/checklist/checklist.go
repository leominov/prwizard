package checklist

import (
	"path"

	"github.com/sirupsen/logrus"

	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/leominov/prwizard/pkg/textutil"
)

const (
	pluginName       = "checklist"
	baseDir          = ".bitbucket"
	templateFileName = "PULL_REQUEST_CHECKLIST.md"
)

func init() {
	plugins.RegisterPullRequestHandler(pluginName, handlePullRequestEvent)
}

type stashClient interface {
	GetAffectedFiles(projectKey, repoSlug string, prID int) ([]string, error)
	CreateComment(project, repo string, prID int, comment string) (*stash.PullRequestCommentResponse, error)
	AddTask(repositoryID, prID, commentID int, text string) error
}

func handlePullRequestEvent(pc plugins.PluginClient, pre stash.PullRequestEvent) error {
	botName, err := pc.StashClient.BotName()
	if err != nil {
		return err
	}
	if pre.Actor.Slug == botName {
		return nil
	}
	if pre.EventKey != "pr:opened" {
		return nil
	}

	templatePath := path.Join(baseDir, templateFileName)
	b, err := pc.StashClient.GetRawFile(
		pre.PullRequest.ToRef.Repository.Project.Key,
		pre.PullRequest.ToRef.Repository.Slug,
		templatePath,
		pre.PullRequest.ToRef.LatestCommit,
	)
	if err != nil {
		if err == stash.ErrFileNotFound {
			return nil
		}
		return err
	}

	checklist := string(b)
	checklist, err = textutil.GenerateTemplate(checklist, "checklist", map[string]interface{}{
		"pr":     pre.PullRequest,
		"author": pre.Actor,
	})
	if err != nil {
		return err
	}

	config := Parse(checklist)

	return handle(pc.Logger, pc.StashClient, config, pre)
}

func handle(log *logrus.Entry, stashCli stashClient, config *Configuration, pre stash.PullRequestEvent) error {
	checks := config.Checks

	if config.RequiredAffectedFiles() {
		affectedFiles, err := stashCli.GetAffectedFiles(
			pre.PullRequest.ToRef.Repository.Project.Key,
			pre.PullRequest.ToRef.Repository.Slug,
			pre.PullRequest.ID,
		)
		if err != nil {
			return err
		}
		checks = config.ChecksByAffectedFiles(affectedFiles)
	}

	checks = UniqueChecks(checks)

	if len(checks) == 0 {
		return nil
	}

	prComment, err := stashCli.CreateComment(
		pre.PullRequest.ToRef.Repository.Project.Key,
		pre.PullRequest.ToRef.Repository.Slug,
		pre.PullRequest.ID,
		config.Message,
	)
	if err != nil {
		return err
	}

	for _, check := range checks {
		err = stashCli.AddTask(
			pre.PullRequest.ToRef.Repository.ID,
			pre.PullRequest.ID,
			prComment.ID,
			check.Name,
		)
		if err != nil {
			log.Error(err)
		}
	}

	return nil
}
