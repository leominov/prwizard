package checklist

func UniqueChecks(checks []*Check) []*Check {
	var list []*Check
	keys := make(map[string]struct{})
	for _, entry := range checks {
		if _, value := keys[entry.Name]; !value {
			keys[entry.Name] = struct{}{}
			list = append(list, entry)
		}
	}
	return list
}
