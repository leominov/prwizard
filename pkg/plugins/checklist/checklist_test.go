package checklist

import (
	"errors"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"github.com/leominov/prwizard/pkg/stash"
)

type fakeStashClient struct {
	affectedFilesRequested bool
	affectedFilesErr       error
	affectedFiles          []string
	comments               []string
	tasks                  []string
}

func (f *fakeStashClient) flush() {
	f.affectedFilesRequested = false
	f.affectedFilesErr = nil
	f.affectedFiles = []string{}
	f.comments = []string{}
	f.tasks = []string{}
}

func (f *fakeStashClient) GetAffectedFiles(projectKey, repoSlug string, prID int) ([]string, error) {
	f.affectedFilesRequested = true
	return f.affectedFiles, f.affectedFilesErr
}

func (f *fakeStashClient) CreateComment(project, repo string, prID int, comment string) (*stash.PullRequestCommentResponse, error) {
	if len(comment) == 0 {
		return nil, errors.New("empty comment")
	}
	f.comments = append(f.comments, comment)
	return &stash.PullRequestCommentResponse{
		ID: len(f.comments),
	}, nil
}

func (f *fakeStashClient) AddTask(repositoryID, prID, commentID int, text string) error {
	if len(text) == 0 {
		return errors.New("empty text")
	}
	f.tasks = append(f.tasks, text)
	return nil
}

func TestHandle(t *testing.T) {
	tests := []struct {
		config                         *Configuration
		affectedFilesErr               error
		affectedFilesRequested         bool
		affectedFiles, comments, tasks []string
		wantErr                        bool
	}{
		{
			config: &Configuration{
				Message: "Hello",
				Checks: []*Check{
					{
						Name: "Task1",
					},
					{
						Name: "Task2",
					},
				},
			},
			comments: []string{
				"Hello",
			},
			tasks: []string{
				"Task1",
				"Task2",
			},
		},
		{
			config: &Configuration{
				Checks: []*Check{
					{
						Name: "Task1",
					},
					{
						Name: "Task2",
					},
				},
			},
			// no message
			wantErr: true,
		},
		{
			config: &Configuration{
				Checks: []*Check{
					{
						Name: "Task1",
						OnChange: []string{
							"foobar/*",
						},
					},
				},
			},
			affectedFilesErr: errors.New("failed to get list of affected files"),
			// no message
			wantErr: true,
		},
		{
			config: &Configuration{
				Message: "Hello",
				Checks: []*Check{
					{
						Name: "Task1",
						OnChange: []string{
							"foobar/abc",
						},
					},
					{
						Name: "Task2",
						OnChange: []string{
							"foobar/*",
							"barfoo/*",
						},
					},
				},
			},
			affectedFilesRequested: true,
			affectedFiles: []string{
				"foobar/abc",
				"foobar/def",
				"foobar/ghi",
				"barfoo/abc",
				"barfoo/def",
				"barfoo/ghi",
			},
			comments: []string{
				"Hello",
			},
			tasks: []string{
				"Task1",
				"Task2",
			},
		},
		{
			config: &Configuration{
				Message: messageTemplate,
				Checks: []*Check{
					{
						Name: "Task1",
						OnChange: []string{
							"foobar/abc",
						},
					},
					{
						Name: "Task2",
						OnChange: []string{
							"foobar/*",
						},
					},
				},
			},
			affectedFilesRequested: true,
			affectedFiles: []string{
				"foobar/def",
			},
			comments: []string{
				messageTemplate,
			},
			tasks: []string{
				"Task2",
			},
		},
		{
			config: &Configuration{
				Message: "Hello",
				Checks: []*Check{
					{
						Name: "Task1",
						OnChange: []string{
							"foobar/abc",
						},
					},
					{
						Name: "Task2",
						OnChange: []string{
							"foobar/*",
						},
					},
				},
			},
			affectedFilesRequested: true,
			affectedFiles:          []string{},
			comments:               []string{},
			tasks:                  []string{},
		},
		{
			config: &Configuration{
				Message: "Hello",
				Checks: []*Check{
					{
						Name: "Task1",
						OnChange: []string{
							"foobar/abc",
						},
					},
					{
						Name: "Task2",
						OnChange: []string{
							"foobar/*",
						},
					},
				},
			},
			affectedFilesRequested: true,
			affectedFiles: []string{
				"abcdef",
				"ghijk",
			},
			comments: []string{},
			tasks:    []string{},
		},
	}
	pre := stash.PullRequestEvent{
		PullRequest: stash.PullRequest{
			ID: 1,
			ToRef: &stash.Ref{
				Repository: stash.Repository{
					ID:   1,
					Slug: "foobar",
					Project: stash.Project{
						Key: "TEST",
					},
				},
			},
		},
	}
	log := logrus.NewEntry(logrus.New())
	fok := &fakeStashClient{}
	for _, test := range tests {
		fok.flush()
		fok.affectedFilesErr = test.affectedFilesErr
		fok.affectedFiles = test.affectedFiles
		err := handle(log, fok, test.config, pre)
		if test.wantErr {
			assert.Error(t, err)
			continue
		}
		assert.NoError(t, err)
		assert.Equal(t, test.affectedFilesRequested, fok.affectedFilesRequested)
		assert.Equal(t, test.comments, fok.comments)
		assert.Equal(t, test.tasks, fok.tasks)
	}
}
