package checklist

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUniqueChecks(t *testing.T) {
	tests := []struct {
		in  []*Check
		out []*Check
	}{
		{
			in:  nil,
			out: nil,
		},
		{
			in:  []*Check{},
			out: nil,
		},
		{
			in: []*Check{
				{
					Name: "foobar",
				},
			},
			out: []*Check{
				{
					Name: "foobar",
				},
			},
		},
		{
			in: []*Check{
				{
					Name: "foobar",
				},
				{
					Name: "foobar",
				},
				{
					Name: "barfoo",
				},
			},
			out: []*Check{
				{
					Name: "foobar",
				},
				{
					Name: "barfoo",
				},
			},
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, UniqueChecks(test.in))
	}
}
