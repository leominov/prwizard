package checklist

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {
	tests := []struct {
		in  string
		out *Configuration
	}{
		{
			in: "",
			out: &Configuration{
				Message: messageTemplate,
				Checks:  nil,
			},
		},
		{
			in: "* Foobar",
			out: &Configuration{
				Message: messageTemplate,
				Checks: []*Check{
					{
						Name: "Foobar",
					},
				},
			},
		},
		{
			in: `Header

* Foobar1

Comment

* Foobar2`,
			out: &Configuration{
				Message: "Header",
				Checks: []*Check{
					{
						Name: "Foobar1",
					},
					{
						Name: "Foobar2",
					},
				},
			},
		},
		{
			in: `---
message: Header
checks:
  - name: Foobar1
    on_change: foobar/*
  - Foobar2`,
			out: &Configuration{
				Message: "Header",
				Checks: []*Check{
					{
						Name: "Foobar1",
						OnChange: []string{
							"foobar/*",
						},
					},
					{
						Name: "Foobar2",
					},
				},
			},
		},
		{
			in: `---
message: Header
checks:
  - name: Foobar1
    on_change:
      - foobar/*
  - Foobar2`,
			out: &Configuration{
				Message: "Header",
				Checks: []*Check{
					{
						Name: "Foobar1",
						OnChange: []string{
							"foobar/*",
						},
					},
					{
						Name: "Foobar2",
					},
				},
			},
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, Parse(test.in))
	}
}

func TestConfiguration_RequiredAffectedFiles(t *testing.T) {
	tests := []struct {
		in      *Configuration
		require bool
	}{
		{
			in:      &Configuration{},
			require: false,
		},
		{
			in: &Configuration{
				Checks: []*Check{},
			},
			require: false,
		},
		{
			in: &Configuration{
				Checks: []*Check{
					{
						Name: "Foobar",
					},
				},
			},
			require: false,
		},
		{
			in: &Configuration{
				Checks: []*Check{
					{
						Name:     "Foobar",
						OnChange: []string{},
					},
				},
			},
			require: false,
		},
		{
			in: &Configuration{
				Checks: []*Check{
					{
						Name:     "Foobar",
						OnChange: nil,
					},
				},
			},
			require: false,
		},
		{
			in: &Configuration{
				Checks: []*Check{
					{
						Name: "Foobar",
						OnChange: []string{
							"foobar/*",
						},
					},
				},
			},
			require: true,
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.require, test.in.RequiredAffectedFiles())
	}
}

func TestConfiguration_ChecksByAffectedFiles(t *testing.T) {
	tests := []struct {
		files []string
		in    *Configuration
		out   []*Check
	}{
		{
			in:  &Configuration{},
			out: nil,
		},
		{
			in: &Configuration{
				Checks: []*Check{
					{
						Name: "Foobar",
					},
				},
			},
			out: []*Check{
				{
					Name: "Foobar",
				},
			},
		},
		{
			in: &Configuration{
				Checks: []*Check{
					{
						Name: "Foobar",
						OnChange: []string{
							"foobar/*",
						},
					},
				},
			},
			out: nil,
		},
		{
			files: []string{
				"foobar/abc",
			},
			in: &Configuration{
				Checks: []*Check{
					{
						Name: "Foobar",
						OnChange: []string{
							"foobar/*",
						},
					},
				},
			},
			out: []*Check{
				{
					Name: "Foobar",
					OnChange: []string{
						"foobar/*",
					},
				},
			},
		},
		{
			files: []string{
				"foobar/abc",
				"foobar/def",
			},
			in: &Configuration{
				Checks: []*Check{
					{
						Name: "Foobar",
						OnChange: []string{
							"foobar/*",
						},
					},
				},
			},
			out: []*Check{
				{
					Name: "Foobar",
					OnChange: []string{
						"foobar/*",
					},
				},
			},
		},
		{
			files: []string{
				"foobar/abc",
				"foobar/def",
			},
			in: &Configuration{
				Checks: []*Check{
					{
						Name: "Foobar",
						OnChange: []string{
							"barfoo/*",
						},
					},
				},
			},
			out: nil,
		},
		{
			files: []string{
				"foobar/abc",
				"foobar/def",
			},
			in: &Configuration{
				Checks: []*Check{
					{
						Name: "Foobar",
						OnChange: []string{
							"barfoo/*",
							"foobar/abc",
						},
					},
				},
			},
			out: []*Check{
				{
					Name: "Foobar",
					OnChange: []string{
						"barfoo/*",
						"foobar/abc",
					},
				},
			},
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.in.ChecksByAffectedFiles(test.files))
	}
}
