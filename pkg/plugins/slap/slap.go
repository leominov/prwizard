package slap

import (
	"fmt"

	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/leominov/prwizard/pkg/textutil"
)

const (
	pluginName      = "slap"
	commandSlap     = "/slap"
	messageTemplate = "@%s slapped you in PR: %s"
)

func init() {
	plugins.RegisterPullRequestCommentHandler(pluginName, handlePullRequestCommentEvent)
}

func isCommentAuthorInReviewers(prc stash.PullRequestCommentEvent) bool {
	for _, rev := range prc.PullRequest.Participants {
		if rev.User.Slug == prc.Comment.Author.Slug {
			return true
		}
	}
	for _, rev := range prc.PullRequest.Reviewers {
		if rev.User.Slug == prc.Comment.Author.Slug {
			return true
		}
	}
	return false
}

func handlePullRequestCommentEvent(pc plugins.PluginClient, prc stash.PullRequestCommentEvent) error {
	botName, err := pc.StashClient.BotName()
	if err != nil {
		return err
	}
	if prc.Comment.Author.Slug == botName {
		return nil
	}
	if !textutil.TextHasCommand(prc.Comment.Text, commandSlap) {
		return nil
	}
	if !isCommentAuthorInReviewers(prc) {
		return nil
	}
	prLink := fmt.Sprintf("%s%s", pc.StashClient.Endpoint(), prc.PullRequest.Path())
	message := fmt.Sprintf(messageTemplate, prc.Comment.Author.Slug, prLink)
	err = pc.SlackClient.WriteMessage(message, fmt.Sprintf("@%s", prc.PullRequest.Author.User.Slug))
	return err
}
