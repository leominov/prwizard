package prtemplate

import (
	"fmt"
	"path"
	"strings"

	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/leominov/prwizard/pkg/textutil"
)

const (
	pluginName       = "prtemplate"
	baseDir          = ".bitbucket"
	templateFileName = "PULL_REQUEST_TEMPLATE.md"
	messageTemplate  = `:boom: В файле ` + "`%s`" + ` расположен шаблон pull-request:

` + "```\n%s\n```" + ``
)

func init() {
	plugins.RegisterPullRequestHandler(pluginName, handlePullRequestEvent)
}

func handlePullRequestEvent(pc plugins.PluginClient, pre stash.PullRequestEvent) error {
	botName, err := pc.StashClient.BotName()
	if err != nil {
		return err
	}
	if pre.Actor.Slug == botName {
		return nil
	}
	if pre.EventKey != "pr:opened" && len(pre.PullRequest.Description) > 0 {
		return nil
	}
	templatePath := path.Join(baseDir, templateFileName)
	b, err := pc.StashClient.GetRawFile(
		pre.PullRequest.ToRef.Repository.Project.Key,
		pre.PullRequest.ToRef.Repository.Slug,
		templatePath,
		pre.PullRequest.ToRef.LatestCommit,
	)
	if err == stash.ErrFileNotFound {
		return nil
	}
	if err != nil {
		return err
	}
	templateText := string(b)
	templateText = strings.TrimSpace(templateText)
	templateText, err = textutil.GenerateTemplate(templateText, "prtemplate", map[string]interface{}{
		"pr":     pre.PullRequest,
		"author": pre.Actor,
	})
	if err != nil {
		return err
	}
	if len(templateText) == 0 {
		return nil
	}
	// if pre.EventKey == "pr:opened" && len(pre.PullRequest.Description) != 0 {
	if pre.EventKey == "pr:opened" {
		_, err = pc.StashClient.CreateComment(
			pre.PullRequest.ToRef.Repository.Project.Key,
			pre.PullRequest.ToRef.Repository.Slug,
			pre.PullRequest.ID,
			fmt.Sprintf(messageTemplate, templatePath, templateText),
		)
		if err != nil {
			return err
		}
		// } else if len(pre.PullRequest.Description) == 0 {
		// 	pre.PullRequest.Description = templateText
		// 	err = pc.StashClient.UpdatePullRequest(
		// 		pre.PullRequest.ToRef.Repository.Project.Key,
		// 		pre.PullRequest.ToRef.Repository.Slug,
		// 		"description",
		// 		pre.PullRequest,
		// 	)
		// 	if err != nil {
		// 		return err
		// 	}
	}
	return nil
}
