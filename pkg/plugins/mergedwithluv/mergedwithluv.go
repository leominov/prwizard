package mergedwithluv

import (
	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
)

const (
	pluginName  = "mergedwithluv"
	messageText = "😍"
)

func init() {
	plugins.RegisterPullRequestHandler(pluginName, handlePullRequestEvent)
}

func handlePullRequestEvent(pc plugins.PluginClient, pre stash.PullRequestEvent) error {
	if pre.EventKey != "pr:merged" {
		return nil
	}
	_, err := pc.StashClient.CreateComment(
		pre.PullRequest.ToRef.Repository.Project.Key,
		pre.PullRequest.ToRef.Repository.Slug,
		pre.PullRequest.ID,
		messageText,
	)
	return err
}
