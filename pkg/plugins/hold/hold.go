package hold

import (
	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/leominov/prwizard/pkg/textutil"
)

const (
	pluginName        = "hold"
	commandHold       = "/hold"
	commandHoldCancel = "/hold cancel"
)

func init() {
	plugins.RegisterPullRequestCommentHandler(pluginName, handlePullRequestCommentEvent)
}

func isCommentAuthorInReviewers(prc stash.PullRequestCommentEvent) bool {
	for _, rev := range prc.PullRequest.Participants {
		if rev.User.Slug == prc.Comment.Author.Slug {
			return true
		}
	}
	for _, rev := range prc.PullRequest.Reviewers {
		if rev.User.Slug == prc.Comment.Author.Slug {
			return true
		}
	}
	return false
}

func handlePullRequestCommentEvent(pc plugins.PluginClient, prc stash.PullRequestCommentEvent) error {
	botName, err := pc.StashClient.BotName()
	if err != nil {
		return err
	}
	if prc.Comment.Author.Slug == botName {
		return nil
	}
	if !isCommentAuthorInReviewers(prc) {
		return nil
	}
	if textutil.TextHasCommand(prc.Comment.Text, commandHold) {
		return pc.StashClient.SetNeedsWork(
			prc.PullRequest.ToRef.Repository.Project.Key,
			prc.PullRequest.ToRef.Repository.Slug,
			prc.PullRequest.ID,
			botName,
		)
	}
	if textutil.TextHasCommand(prc.Comment.Text, commandHoldCancel) {
		return pc.StashClient.DeleteReviewer(
			prc.PullRequest.ToRef.Repository.Project.Key,
			prc.PullRequest.ToRef.Repository.Slug,
			prc.PullRequest.ID,
			botName,
		)
	}
	return nil
}
