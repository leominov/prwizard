package repoowners

import (
	"testing"

	"github.com/leominov/prwizard/pkg/textutil"
)

func TestReplaceAliases(t *testing.T) {
	tests := []struct {
		repoOwners   *RepoOwners
		resultOwners []string
	}{
		{
			repoOwners: &RepoOwners{
				rules: []*OwnerRule{
					&OwnerRule{
						Mask:   "**",
						Owners: []string{"a", "b", "c"},
					},
				},
				aliases: make(map[string][]string),
			},
			resultOwners: []string{"a", "b", "c"},
		},
		{
			repoOwners: &RepoOwners{
				rules: []*OwnerRule{
					&OwnerRule{
						Mask:   "**",
						Owners: []string{"a", "b", "c", "c"},
					},
				},
				aliases: map[string][]string{
					"a": []string{"d", "e"},
				},
			},
			resultOwners: []string{"e", "b", "c", "d"},
		},
	}
	for _, test := range tests {
		owners := test.repoOwners.ReplaceAliases(test.repoOwners.rules[0])
		if !textutil.IsSimilarStringSlice(owners, test.resultOwners) {
			t.Errorf("Must be %v, but got %v", test.resultOwners, owners)
		}
	}
}

func TestFindOwnersForFiles(t *testing.T) {
	config := &RepoOwners{
		rules: []*OwnerRule{
			&OwnerRule{
				Mask:   "FOOBAR/**",
				Owners: []string{"a"},
			},
			&OwnerRule{
				Mask:   "FOOBAR",
				Owners: []string{"b"},
			},
		},
		aliases: map[string][]string{
			"a": []string{"b", "c"},
		},
	}
	tests := []struct {
		filepath string
		owners   []string
	}{
		{
			filepath: "README.md",
			owners:   []string{},
		},
		{
			filepath: "FOOBAR/README.md",
			owners:   []string{"b", "c"},
		},
	}
	for _, test := range tests {
		owners := config.FindOwnersForFiles([]string{test.filepath})
		if !textutil.IsSimilarStringSlice(owners, test.owners) {
			t.Errorf("Must be %v, but got %v", test.owners, owners)
		}
	}
}

func TestParseFileContent(t *testing.T) {
	tests := []struct {
		content string
		rules   int
	}{
		{
			content: `# abcd

README.md
** @foobar`,
			rules: 1,
		},
		{
			content: `README.md @foobar
** @foobar`,
			rules: 2,
		},
	}
	for _, test := range tests {
		rules := ParseFileContent([]byte(test.content))
		if len(rules) != test.rules {
			t.Errorf("Must be %d, but got %d", test.rules, len(rules))
		}
	}
}
