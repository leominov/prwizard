package repoowners

import (
	"bufio"
	"bytes"

	"strings"

	"github.com/cloudfoundry/cli/util/glob"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

const (
	ownersFileName  = "OWNERS"
	aliasesFileName = "OWNERS_ALIASES"
)

type Client struct {
	logger *logrus.Entry

	sc *stash.Client
}

type OwnerRule struct {
	Mask   string
	Owners []string
}

func NewClient(sc *stash.Client) *Client {
	return &Client{
		logger: logrus.WithField("component", "repoowners"),
		sc:     sc,
	}
}

func (c *Client) LoadRepoOwners(project, repo, base string) (*RepoOwners, error) {
	result := &RepoOwners{
		rules:   []*OwnerRule{},
		aliases: make(map[string][]string),
	}
	// Try to load group aliases from another repo file
	raw, err := c.sc.GetRawFile(project, repo, aliasesFileName, base)
	if err == nil {
		_ = yaml.Unmarshal(raw, &result.aliases)
	}
	raw, err = c.sc.GetRawFile(project, repo, ownersFileName, base)
	if err != nil {
		return nil, err
	}
	rules := ParseFileContent(raw)
	result.rules = rules
	return result, nil
}

func ParseFileContent(raw []byte) []*OwnerRule {
	rules := []*OwnerRule{}
	s := bufio.NewScanner(bytes.NewReader(raw))
	for line := 1; s.Scan(); line++ {
		ownerRuleRaw := strings.TrimSpace(s.Text())
		if len(ownerRuleRaw) == 0 {
			continue
		}
		if strings.HasPrefix(ownerRuleRaw, "#") {
			continue
		}
		ownerRuleArgs := strings.Split(ownerRuleRaw, " ")
		if len(ownerRuleArgs) == 1 {
			continue
		}
		owr := &OwnerRule{
			Mask:   ownerRuleArgs[0],
			Owners: ownerRuleArgs[1:],
		}
		for i, owner := range owr.Owners {
			owr.Owners[i] = strings.TrimPrefix(owner, "@")
		}
		rules = append(rules, owr)
	}
	return rules
}

type RepoOwners struct {
	rules   []*OwnerRule
	aliases map[string][]string
}

func (r *RepoOwners) ReplaceAliases(rule *OwnerRule) []string {
	if len(r.aliases) == 0 {
		return rule.Owners
	}
	ownersList := []string{}
	for _, owner := range rule.Owners {
		match := false
		for alias, ownrs := range r.aliases {
			if owner == alias {
				match = true
				ownersList = append(ownersList, ownrs...)
				break
			}
		}
		if !match {
			ownersList = append(ownersList, owner)
		}
	}
	ownersList = removeDuplicatesUnordered(ownersList)
	return ownersList
}

func (r *RepoOwners) FindOwnersForFile(file string) []string {
	for _, rule := range r.rules {
		gl := glob.MustCompileGlob(rule.Mask)
		if gl.Match(file) {
			owns := removeDuplicatesUnordered(rule.Owners)
			owns = r.ReplaceAliases(rule)
			return owns
		}
	}
	return []string{}
}

func (r *RepoOwners) FindOwnersForFiles(files []string) []string {
	var ownersList []string
	for _, fileName := range files {
		ownersList = append(ownersList, r.FindOwnersForFile(fileName)...)
	}
	ownersList = removeDuplicatesUnordered(ownersList)
	return ownersList
}

func removeDuplicatesUnordered(elements []string) []string {
	encountered := map[string]bool{}
	for v := range elements {
		encountered[elements[v]] = true
	}
	result := []string{}
	for key := range encountered {
		result = append(result, key)
	}
	return result
}
