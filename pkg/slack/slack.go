package slack

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	apiURL = "https://slack.com/api/"

	authTest = apiURL + "auth.test"
	apiTest  = apiURL + "api.test"

	chatPostMessage = apiURL + "chat.postMessage"

	botName      = "korjik"
	botIconEmoji = ":robot_face:"
)

var (
	defaultTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}
	defaultHTTPClient = &http.Client{
		Timeout:   time.Second * 10,
		Transport: defaultTransport,
	}
)

// Client is Slack client
type Client struct {
	logger         *logrus.Entry
	token          string
	postMessageURL string
	fake           bool
}

// APIResponse for VerifyAPI
type APIResponse struct {
	Ok bool `json:"ok"`
}

// AuthResponse for VerifyAuth
type AuthResponse struct {
	Ok     bool   `json:"ok"`
	URL    string `json:"url"`
	Team   string `json:"team"`
	User   string `json:"user"`
	TeamID string `json:"team_id"`
	UserID string `json:"user_id"`
}

// NewClient returns Slack client
func NewClient() *Client {
	return &Client{
		postMessageURL: chatPostMessage,
		logger:         logrus.WithField("client", "slack"),
	}
}

// NewFakeClient return fake Slack client
func NewFakeClient() *Client {
	return &Client{
		fake: true,
	}
}

func (sl *Client) SetToken(token string) *Client {
	sl.token = token
	return sl
}

func (sl *Client) SetPostMessageURL(u string) *Client {
	sl.postMessageURL = u
	return sl
}

func (sl *Client) log(methodName string, args ...interface{}) {
	if sl.logger == nil {
		return
	}
	var as []string
	for _, arg := range args {
		as = append(as, fmt.Sprintf("%v", arg))
	}
	sl.logger.Infof("%s(%s)", methodName, strings.Join(as, ", "))
}

// VerifyAPI to verify Slack API
func (sl *Client) VerifyAPI() (bool, error) {
	sl.log("VerifyAPI")
	if sl.fake {
		return true, nil
	}
	t, e := sl.postMessage(apiTest, sl.urlValues())
	if e != nil {
		return false, e
	}

	var apiResponse APIResponse
	e = json.Unmarshal(t, &apiResponse)
	if e != nil {
		return false, e
	}
	return apiResponse.Ok, nil
}

// VerifyAuth to verify user auth
func (sl *Client) VerifyAuth() (bool, error) {
	sl.log("VerifyAuth")
	if sl.fake {
		return true, nil
	}
	t, e := sl.postMessage(authTest, sl.urlValues())
	if e != nil {
		return false, e
	}

	var authResponse AuthResponse
	e = json.Unmarshal(t, &authResponse)
	if e != nil {
		return false, e
	}
	return authResponse.Ok, nil
}

func (sl *Client) urlValues() *url.Values {
	uv := url.Values{}
	uv.Add("username", botName)
	uv.Add("icon_emoji", botIconEmoji)

	if len(sl.token) > 0 {
		uv.Add("token", sl.token)
	}
	return &uv
}

func (sl *Client) postMessage(url string, uv *url.Values) ([]byte, error) {
	resp, err := defaultHTTPClient.PostForm(url, *uv)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		t, _ := ioutil.ReadAll(resp.Body)
		return nil, errors.New(string(t))
	}
	t, _ := ioutil.ReadAll(resp.Body)
	return t, nil
}

type SlackRequest struct {
	Channel     string            `json:"channel,omitempty"`
	Username    string            `json:"username,omitempty"`
	IconEmoji   string            `json:"icon_emoji,omitempty"`
	IconURL     string            `json:"icon_url,omitempty"`
	LinkNames   bool              `json:"link_names,omitempty"`
	Attachments []SlackAttachment `json:"attachments"`
}

type SlackAttachment struct {
	Text     string   `json:"text"`
	MrkdwnIn []string `json:"mrkdwn_in,omitempty"`
}

func (sl *Client) writeAsAPICall(text, channel string) error {
	var uv *url.Values = sl.urlValues()
	uv.Add("channel", channel)
	uv.Add("text", text)
	uv.Add("link_names", "true")

	_, err := sl.postMessage(sl.postMessageURL, uv)
	return err
}

func (sl *Client) writeAsAPIURLCall(text, channel string) error {
	req := &SlackRequest{
		Channel:   channel,
		Username:  "Pull Reminders",
		LinkNames: true,
		Attachments: []SlackAttachment{
			SlackAttachment{
				Text:     text,
				MrkdwnIn: []string{"fallback", "pretext", "text"},
			},
		},
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req); err != nil {
		return err
	}
	resp, err := defaultHTTPClient.Post(sl.postMessageURL, "application/json", &buf)
	if err != nil {
		return err
	}
	err = resp.Body.Close()
	if err != nil {
		return err
	}
	return nil
}

// WriteMessage sends message to Slack
func (sl *Client) WriteMessage(text, channel string) error {
	sl.log("WriteMessage", text, channel)
	if sl.fake {
		return nil
	}

	if len(sl.token) == 0 {
		return sl.writeAsAPIURLCall(text, channel)
	}

	return sl.writeAsAPICall(text, channel)
}
