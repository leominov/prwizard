package textutil

import (
	"bufio"
	"bytes"
	"fmt"
	"strings"
	"text/template"
)

const (
	commandPrefix = "/"
)

func SplitTextByLine(text string) []string {
	var result []string
	scanner := bufio.NewScanner(strings.NewReader(text))
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.TrimSpace(line)
		if len(line) == 0 {
			continue
		}
		result = append(result, line)
	}
	return result
}

func TextHasCommand(text, command string) bool {
	lines := SplitTextByLine(text)
	for _, line := range lines {
		if !strings.HasPrefix(line, commandPrefix) {
			continue
		}
		if strings.ToLower(line) == command {
			return true
		}
	}
	return false
}

func GenerateTemplate(templ, name string, data interface{}) (string, error) {
	buf := bytes.NewBufferString("")
	if messageTempl, err := template.New(name).Parse(templ); err != nil {
		return "", fmt.Errorf("failed to parse template for %s: %v", name, err)
	} else if err := messageTempl.Execute(buf, data); err != nil {
		return "", fmt.Errorf("failed to execute template for %s: %v", name, err)
	}
	return buf.String(), nil
}

func IsSimilarStringSlice(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for _, labelA := range a {
		found := false
		for _, labelB := range b {
			if labelA == labelB {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}
