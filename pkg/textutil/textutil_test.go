package textutil

import "testing"

func TestIsSimilarStringSlice(t *testing.T) {
	tests := []struct {
		a, b []string
		want bool
	}{
		{
			a:    []string{},
			b:    []string{},
			want: true,
		},
		{
			a:    []string{"a", "b"},
			b:    []string{},
			want: false,
		},
		{
			a:    []string{"a", "b"},
			b:    []string{"c", "d"},
			want: false,
		},
		{
			a:    []string{"a", "b"},
			b:    []string{"b", "a"},
			want: true,
		},
	}
	for _, test := range tests {
		result := IsSimilarStringSlice(test.a, test.b)
		if result != test.want {
			t.Errorf("IsSimilarStringSlice() = %v, want = %v", result, test.want)
		}
	}
}

func TestSplitTextByLine(t *testing.T) {
	result := SplitTextByLine(`yohoho

yohoho`)
	if len(result) == 0 {
		t.Error("SplitTextByLine() = 0")
	}
}

func TestTextHasCommand(t *testing.T) {
	tests := []struct {
		text, command string
		found         bool
	}{
		{
			text:    "foobar",
			command: "/foobar",
			found:   false,
		},
		{
			text:    "/foobar",
			command: "/foobar",
			found:   true,
		},
	}
	for _, test := range tests {
		result := TextHasCommand(test.text, test.command)
		if result != test.found {
			t.Errorf("TextHasCommand(%s) = %v, want = %v", test.text, result, test.found)
		}
	}
}
