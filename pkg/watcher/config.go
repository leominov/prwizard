package watcher

import "time"

type Config struct {
	Rules          []*Rule       `yaml:"rules"`
	NoticeText     string        `yaml:"noticeText"`
	DeclineText    string        `yaml:"declineText"`
	RepeatInterval time.Duration `yaml:"interval"`
}

type Rule struct {
	ProjectKey string        `yaml:"projectKey"`
	RepoSlug   string        `yaml:"repoSlug"`
	TTL        time.Duration `yaml:"ttl"`
}
