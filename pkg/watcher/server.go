package watcher

import (
	"strings"
	"time"

	"github.com/leominov/prwizard/pkg/stash"
	"github.com/leominov/prwizard/pkg/textutil"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

const (
	declineMark = "#marked-to-decline"
)

type Server struct {
	BotName     string
	ProjectKey  string
	RepoSlug    string
	StashClient *stash.Client
}

type Repository struct {
	Repo *stash.Repository
	Rule *Rule
}

type PullRequest struct {
	PR   *stash.PullRequest
	Rule *Rule
}

func (s *Server) loadConfig() (*Config, error) {
	raw, err := s.StashClient.GetRawFile(s.ProjectKey, s.RepoSlug, ".watcher.yaml", "master")
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = yaml.Unmarshal(raw, &config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

func (s *Server) getManagedRepos(rules []*Rule) []*Repository {
	var repos []*Repository
	for _, rule := range rules {
		if len(rule.RepoSlug) == 0 {
			ruleRepos, err := s.StashClient.GetRepos(rule.ProjectKey)
			if err != nil {
				continue
			}
			for _, ruleRepo := range ruleRepos {
				repos = append(repos, &Repository{ruleRepo, rule})
			}
		} else {
			ruleRepo, err := s.StashClient.GetRepo(rule.ProjectKey, rule.RepoSlug)
			if err != nil {
				continue
			}
			repos = append(repos, &Repository{ruleRepo, rule})
		}
	}
	return repos
}

func (s *Server) getManagedPullRequests(rules []*Rule) []*PullRequest {
	var pullRequests []*PullRequest
	repos := s.getManagedRepos(rules)
	for _, repo := range repos {
		prs, err := s.StashClient.GetPullRequests(repo.Repo.Project.Key, repo.Repo.Slug)
		if err != nil {
			continue
		}
		for _, pr := range prs {
			if !pr.Open {
				continue
			}
			if time.Now().After(pr.GetUpdatedDate().Add(repo.Rule.TTL)) {
				pullRequests = append(pullRequests, &PullRequest{pr, repo.Rule})
			}
		}
	}
	return pullRequests
}

func (s *Server) hasRecentActivities(acts []*stash.Activity, ttl time.Duration) bool {
	if len(acts) == 0 {
		return false
	}
	lastActivity := acts[0]
	if time.Now().After(lastActivity.GetCreatedDate().Add(ttl)) {
		return true
	}
	return false
}

func (s *Server) hasDeclineMark(acts []*stash.Activity) bool {
	if len(acts) == 0 {
		return false
	}
	lastActivity := acts[0]
	if lastActivity.User.Slug != s.BotName {
		return false
	}
	if lastActivity.Action != "COMMENTED" {
		return false
	}
	if lastActivity.CommentAction != "ADDED" {
		return false
	}
	if lastActivity.Comment == nil {
		return false
	}
	if !strings.Contains(lastActivity.Comment.Text, declineMark) {
		return false
	}
	return true
}

func (s *Server) addComment(pr *stash.PullRequest, msg string) error {
	templateText := strings.TrimSpace(msg)
	templateText, err := textutil.GenerateTemplate(templateText, "prtemplate", map[string]interface{}{
		"pr": pr,
	})
	if err != nil {
		return err
	}
	_, err = s.StashClient.CreateComment(
		pr.ToRef.Repository.Project.Key,
		pr.ToRef.Repository.Slug,
		pr.ID,
		templateText,
	)
	return err
}

func (s *Server) checkPullRequests(config *Config) error {
	prs := s.getManagedPullRequests(config.Rules)
	for _, pr := range prs {
		activities, err := s.StashClient.GetActivities(
			pr.PR.ToRef.Repository.Project.Key,
			pr.PR.ToRef.Repository.Slug,
			pr.PR.ID,
		)
		if err != nil {
			logrus.Error(err)
			continue
		}
		if !s.hasRecentActivities(activities, pr.Rule.TTL) {
			continue
		}
		if !s.hasDeclineMark(activities) {
			err := s.addComment(pr.PR, config.NoticeText)
			if err != nil {
				logrus.Error(err)
			}
		} else {
			err = s.StashClient.DeclinePullRequest(
				pr.PR.ToRef.Repository.Project.Key,
				pr.PR.ToRef.Repository.Slug,
				pr.PR.ID,
				pr.PR.Version,
			)
			if err != nil {
				logrus.Error(err)
				continue
			}
			err := s.addComment(pr.PR, config.DeclineText)
			if err != nil {
				logrus.Error(err)
			}
		}
	}
	return nil
}

func (s *Server) bootstrap() error {
	name, err := s.StashClient.BotName()
	if err != nil {
		return err
	}
	s.BotName = name
	return nil
}

func (s *Server) Run() error {
	err := s.bootstrap()
	if err != nil {
		return err
	}
	for {
		config, err := s.loadConfig()
		if err != nil {
			logrus.Error(err)
			time.Sleep(10 * time.Second)
			continue
		}
		if err := s.checkPullRequests(config); err != nil {
			logrus.Error(err)
		}
		time.Sleep(config.RepeatInterval)
	}
}
