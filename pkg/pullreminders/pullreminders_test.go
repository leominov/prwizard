package pullreminders

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/leominov/prwizard/pkg/stash"
)

func TestPullRequestSummary_AsMrkdwn(t *testing.T) {
	tests := []struct {
		sum *PullRequestSummary
		out string
	}{
		{
			sum: &PullRequestSummary{},
			out: "",
		},
		{
			sum: &PullRequestSummary{
				PR: &stash.PullRequest{
					ID: 1,
				},
				Project: &stash.Project{
					Key: "TEST",
				},
				Repo: &stash.Repository{
					Slug: "test",
				},
			},
			out: `[<http://127.0.0.1/projects/TEST|TEST>] <http://127.0.0.1/projects/TEST/repos/test/pull-requests/1|>
a long while ago · Waiting on n/a`,
		},
		{
			sum: &PullRequestSummary{
				PR: &stash.PullRequest{
					ID: 1,
					Reviewers: []*stash.Author{
						{
							Approved: false,
							User: stash.Actor{
								Name: "Lev Aminov",
								Slug: "l.aminov",
							},
						},
						{
							Approved: true,
							User: stash.Actor{
								Name: "Vasya Petrov",
								Slug: "v.petrov",
							},
						},
					},
				},
				Project: &stash.Project{
					Key: "TEST",
				},
				Repo: &stash.Repository{
					Slug: "test",
				},
			},
			out: `[<http://127.0.0.1/projects/TEST|TEST>] <http://127.0.0.1/projects/TEST/repos/test/pull-requests/1|>
a long while ago · Waiting on @l.aminov`,
		},
	}
	for _, test := range tests {
		out := test.sum.AsMrkdwn("http://127.0.0.1")
		assert.Equal(t, test.out, out)
	}
}
