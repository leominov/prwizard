package pullreminders

import (
	"os"
	"strings"
)

type Config struct {
	DryRun        bool
	StashToken    string
	StashEndpoint string
	SlackAPIURL   string
	SlackChannel  string
	ProjectKeys   []string
}

func ConfigFromEnv() *Config {
	c := &Config{
		StashToken:    os.Getenv("STASH_TOKEN"),
		StashEndpoint: os.Getenv("STASH_ENDPOINT"),
		SlackAPIURL:   os.Getenv("SLACK_API_URL"),
		SlackChannel:  os.Getenv("SLACK_CHANNEL"),
		ProjectKeys:   []string{},
	}
	c.StashEndpoint = strings.TrimSuffix(c.StashEndpoint, "/")
	projectKeys := strings.Split(os.Getenv("PROJECT_KEYS"), ",")
	for _, key := range projectKeys {
		key = strings.TrimSpace(key)
		c.ProjectKeys = append(c.ProjectKeys, key)
	}
	return c
}
