package pullreminders

import (
	"fmt"
	"path"
	"strconv"
	"strings"

	humanize "github.com/dustin/go-humanize"
	"github.com/leominov/prwizard/pkg/slack"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/sirupsen/logrus"
)

type PullReminders struct {
	log    *logrus.Entry
	stash  *stash.Client
	slack  *slack.Client
	config *Config
}

func New(log *logrus.Entry, config *Config) (*PullReminders, error) {
	stashClient := stash.NewClient(config.StashEndpoint, nil)
	stashClient.SetCredentials(config.StashToken)
	bot, err := stashClient.BotName()
	if err != nil {
		return nil, err
	}
	log.Infof("Logged as %s", bot)

	slackClient := slack.NewClient().SetPostMessageURL(config.SlackAPIURL)
	if config.DryRun {
		slackClient = slack.NewFakeClient()
	}
	return &PullReminders{
		log:    log,
		stash:  stashClient,
		slack:  slackClient,
		config: config,
	}, nil
}

func (p *PullReminders) Run() error {
	pullRequests := []*PullRequestSummary{}
	for _, pkey := range p.config.ProjectKeys {
		prs, err := p.processProject(pkey)
		if err != nil {
			return err
		}
		pullRequests = append(pullRequests, prs...)
	}
	if len(pullRequests) == 0 {
		return nil
	}
	p.log.Infof("Found PRs: %d", len(pullRequests))
	return p.notify(pullRequests)
}

func (p *PullReminders) notify(prs []*PullRequestSummary) error {
	text := ""
	for _, item := range prs {
		text += item.AsMrkdwn(p.config.StashEndpoint) + "\n\n"
	}
	return p.slack.WriteMessage(text, p.config.SlackChannel)
}

type PullRequestSummary struct {
	PR      *stash.PullRequest
	Project *stash.Project
	Repo    *stash.Repository
}

func (p *PullRequestSummary) AsMrkdwn(baseURL string) string {
	var reviewers []string
	if p.PR == nil || p.Project == nil || p.Repo == nil {
		return ""
	}
	for _, rw := range p.PR.Reviewers {
		if rw.Approved {
			continue
		}
		reviewers = append(reviewers, fmt.Sprintf("@%s", rw.User.Slug))
	}
	if len(reviewers) == 0 {
		reviewers = []string{"n/a"}
	}
	return fmt.Sprintf(
		"[<%s|%s>] <%s|%s>\n%s · Waiting on %s",
		baseURL+path.Join("/projects", p.Project.Key),
		p.Project.Key,
		baseURL+path.Join("/projects", p.Project.Key, "repos", p.Repo.Slug, "pull-requests", strconv.Itoa(p.PR.ID)),
		p.PR.Title,
		humanize.Time(p.PR.GetCreatedDate()),
		strings.Join(reviewers, ", "),
	)
}

func (p *PullReminders) processProject(key string) ([]*PullRequestSummary, error) {
	project, err := p.stash.GetProject(key)
	if err != nil {
		return nil, err
	}
	repos, err := p.stash.GetRepos(key)
	if err != nil {
		return nil, err
	}
	pullRequests := []*PullRequestSummary{}
	for _, repo := range repos {
		prs, err := p.stash.GetPullRequests(key, repo.Slug)
		if err != nil {
			return nil, err
		}
		for _, pr := range prs {
			if pr.Closed {
				continue
			}
			if strings.HasPrefix(pr.Title, "WIP") {
				continue
			}
			pullRequests = append(pullRequests, &PullRequestSummary{
				PR:      pr,
				Project: project,
				Repo:    repo,
			})
		}
	}
	return pullRequests, nil
}
