package grower

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/sirupsen/logrus"
)

type Config struct {
	Webhook *stash.Webhook `yaml:"webhook"`
}

type Server struct {
	Config      *Config
	StashClient *stash.Client
}

func (s *Server) ProjectsHandler(w http.ResponseWriter, r *http.Request) {
	projects, err := s.StashClient.GetProjects()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	encoder := json.NewEncoder(w)
	err = encoder.Encode(projects)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) ProjectHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	project, err := s.StashClient.GetProject(projectKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	encoder := json.NewEncoder(w)
	err = encoder.Encode(project)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) ProjectReposHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repos, err := s.StashClient.GetRepos(projectKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	encoder := json.NewEncoder(w)
	err = encoder.Encode(repos)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) ProjectRepoHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repoSlug := vars["repoSlug"]
	repo, err := s.StashClient.GetRepo(projectKey, repoSlug)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	encoder := json.NewEncoder(w)
	err = encoder.Encode(repo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) RepoWebhooksHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repoSlug := vars["repoSlug"]

	switch r.Method {
	case http.MethodPost:
		err := s.StashClient.AddWebhook(projectKey, repoSlug, s.Config.Webhook)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	case http.MethodGet:
		webhooks, err := s.StashClient.GetWebhooks(projectKey, repoSlug)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		encoder := json.NewEncoder(w)
		err = encoder.Encode(webhooks)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func (s *Server) WebhookHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repoSlug := vars["repoSlug"]
	webhookIDRaw := vars["id"]

	webhookID, err := strconv.Atoi(webhookIDRaw)
	if err != nil {
		http.Error(w, "Incorrent webhookd id type", http.StatusBadRequest)
		return
	}

	switch r.Method {
	case http.MethodGet:
		webhook, err := s.StashClient.GetWebhook(projectKey, repoSlug, webhookID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		encoder := json.NewEncoder(w)
		err = encoder.Encode(webhook)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	case http.MethodDelete:
		err = s.StashClient.DeleteWebhook(projectKey, repoSlug, webhookID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func (s *Server) EnableWebhookHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repoSlug := vars["repoSlug"]
	webhookIDRaw := vars["id"]

	webhookID, err := strconv.Atoi(webhookIDRaw)
	if err != nil {
		http.Error(w, "Incorrent webhookd id type", http.StatusBadRequest)
		return
	}

	err = s.StashClient.SwitchWebhookActive(projectKey, repoSlug, webhookID, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) DisableWebhookHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repoSlug := vars["repoSlug"]
	webhookIDRaw := vars["id"]

	webhookID, err := strconv.Atoi(webhookIDRaw)
	if err != nil {
		http.Error(w, "Incorrent webhookd id type", http.StatusBadRequest)
		return
	}

	err = s.StashClient.SwitchWebhookActive(projectKey, repoSlug, webhookID, false)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) ProjectWebhooksHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]

	repos, err := s.StashClient.GetRepos(projectKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var webhooks []*stash.Webhook
	for _, repo := range repos {
		wh, err := s.StashClient.GetWebhooks(projectKey, repo.Slug)
		if err != nil {
			logrus.Error(err)
			continue
		}
		webhooks = append(webhooks, wh...)
	}

	encoder := json.NewEncoder(w)
	err = encoder.Encode(webhooks)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) RepoPRSettingsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repoSlug := vars["repoSlug"]

	settings, err := s.StashClient.GetRepoPRSettings(projectKey, repoSlug)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	encoder := json.NewEncoder(w)
	err = encoder.Encode(settings)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) RepoTagsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repoSlug := vars["repoSlug"]

	tags, err := s.StashClient.GetTags(projectKey, repoSlug)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	encoder := json.NewEncoder(w)
	err = encoder.Encode(tags)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
