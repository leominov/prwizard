// https://docs.atlassian.com/software/jira/docs/api/REST/7.5.0/
package jira

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	defaultTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}
	defaultHttpClient = &http.Client{
		Timeout:   time.Second * 10,
		Transport: defaultTransport,
	}
)

type Client struct {
	logger     *logrus.Entry
	endpoint   string
	authSecret string
	client     *http.Client
}

func NewClient(endpoint string, httpCli *http.Client) *Client {
	endpoint = strings.TrimRight(endpoint, "/")
	if httpCli == nil {
		httpCli = defaultHttpClient
	}
	return &Client{
		logger:   logrus.WithField("client", "jira"),
		client:   httpCli,
		endpoint: endpoint,
	}
}

// SetCredentials to set up auth token
func (c *Client) SetCredentials(authSecret string) {
	c.authSecret = authSecret
}

// Endpoint returns Stash endpoint
func (c *Client) Endpoint() string {
	return c.endpoint
}

func (c *Client) log(methodName string, args ...interface{}) {
	if c.logger == nil {
		return
	}
	var as []string
	for _, arg := range args {
		as = append(as, fmt.Sprintf("%v", arg))
	}
	c.logger.Infof("%s(%s)", methodName, strings.Join(as, ", "))
}

func (c *Client) doRequest(method, path, accept string, body interface{}) (*http.Response, error) {
	var buf io.Reader
	if body != nil {
		b, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}
		buf = bytes.NewBuffer(b)
	}
	req, err := http.NewRequest(method, path, buf)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", c.authSecret)
	req.Header.Add("Content-Type", "application/json")
	if accept != "" {
		req.Header.Add("Accept", accept)
	}
	req.Close = true
	return c.client.Do(req)
}

func (c *Client) GetIssue(key string) (*Issue, error) {
	c.log("GetIssue", key)
	url := fmt.Sprintf("%s/rest/api/latest/issue/%s?fields=status", c.endpoint, key)
	resp, err := c.doRequest(http.MethodGet, url, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	issue := &Issue{}
	err = json.Unmarshal(b, &issue)
	if err != nil {
		return nil, err
	}
	return issue, nil
}

func (c *Client) CountUnresolvedIssuesByLabel(label string) (int, error) {
	c.log("CountUnresolvedIssuesByLabel", label)
	type cr struct {
		Total int `json:"total"`
	}
	url := fmt.Sprintf(
		"%s/rest/api/2/search?jql=labels=%s+and+resolution=Unresolved&fields=id",
		c.endpoint,
		url.PathEscape(label),
	)
	resp, err := c.doRequest(http.MethodGet, url, "", nil)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	crE := cr{}
	err = json.Unmarshal(b, &crE)
	if err != nil {
		return 0, err
	}
	return crE.Total, nil
}
