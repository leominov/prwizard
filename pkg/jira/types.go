package jira

import (
	"fmt"
)

const (
	IssueResolutionResolved   IssueResolution = "Resolved"
	IssueResolutionUnresolved IssueResolution = "Unresolved"
	IssueResolutionAll        IssueResolution = "All"
)

type IssueResolution string

type Issue struct {
	ID     string `json:"id"`
	Self   string `json:"self"`
	Expand string `json:"expand"`
	Key    string `json:"key"`
	Fileds Fields `json:"fields"`
}

type Fields struct {
	Status Status `json:"status"`
}

type Status struct {
	ID             string         `json:"id"`
	Self           string         `json:"self"`
	Description    string         `json:"description"`
	IconURL        string         `json:"iconUrl"`
	Name           string         `json:"name"`
	StatusCategory StatusCategory `json:"statusCategory"`
}

type StatusCategory struct {
	ID        int    `json:"id"`
	Self      string `json:"self"`
	Key       string `json:"key"`
	ColorName string `json:"colorName"`
	Name      string `json:"name"`
}

func (i *IssueResolution) JQL() string {
	if *i == IssueResolutionAll {
		return ""
	}
	return fmt.Sprintf("resolution=%v", i)
}
