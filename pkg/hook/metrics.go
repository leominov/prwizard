package hook

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	webhookCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "atlas_hook_webhook_counter",
		Help: "A counter of the webhooks made to hook.",
	}, []string{"event_type"})
)

func init() {
	prometheus.MustRegister(webhookCounter)
}

type Metrics struct {
	WebhookCounter *prometheus.CounterVec
}

func NewMetrics() *Metrics {
	return &Metrics{
		WebhookCounter: webhookCounter,
	}
}
