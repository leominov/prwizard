package hook

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"encoding/json"

	"sync"

	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/sirupsen/logrus"
)

type Server struct {
	Plugins    *plugins.PluginAgent
	Metrics    *Metrics
	HMACSecret []byte

	wg sync.WaitGroup
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	eventType, eventGUID, payload, ok := ValidateWebhook(w, r, s.HMACSecret)
	if !ok {
		return
	}
	fmt.Fprint(w, "Event received. Have a nice day.")
	if err := s.demuxEvent(eventType, eventGUID, payload, r.Header); err != nil {
		logrus.WithError(err).Error("Error parsing event.")
	}
}

func ValidateWebhook(w http.ResponseWriter, r *http.Request, hmacSecret []byte) (string, string, []byte, bool) {
	defer r.Body.Close()

	if r.Method == http.MethodGet {
		return "", "", nil, false
	}

	if r.Method != http.MethodPost {
		resp := "405 Method not allowed"
		logrus.Debug(resp)
		http.Error(w, resp, http.StatusMethodNotAllowed)
		return "", "", nil, false
	}
	eventGUID := r.Header.Get("X-Request-Id")
	if eventGUID == "" {
		resp := "400 Bad Request: Missing X-Request-Id Header"
		logrus.Debug(resp)
		http.Error(w, resp, http.StatusBadRequest)
		return "", "", nil, false
	}
	eventType := r.Header.Get("X-Event-Key")
	if eventType == "" {
		resp := "400 Bad Request: Missing X-Event-Key Header"
		logrus.Debug(resp)
		http.Error(w, resp, http.StatusBadRequest)
		return "", "", nil, false
	}
	if eventType == "diagnostics:ping" {
		fmt.Fprint(w, "ok")
		return "", "", nil, false
	}
	sig := r.Header.Get("X-Hub-Signature")
	if sig == "" {
		resp := "403 Forbidden: Missing X-Hub-Signature"
		logrus.Debug(resp)
		http.Error(w, resp, http.StatusForbidden)
		return "", "", nil, false
	}
	defer r.Body.Close()
	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		resp := "500 Internal Server Error: Failed to read request body"
		logrus.Debug(resp)
		http.Error(w, resp, http.StatusInternalServerError)
		return "", "", nil, false
	}

	if !stash.ValidatePayload(payload, sig, hmacSecret) {
		resp := "403 Forbidden: Invalid X-Hub-Signature"
		logrus.Debug(resp)
		http.Error(w, resp, http.StatusForbidden)
		return "", "", nil, false
	}

	return eventType, eventGUID, payload, true
}

func (s *Server) demuxEvent(eventType, eventGUID string, payload []byte, h http.Header) error {
	l := logrus.WithFields(
		logrus.Fields{
			"event-type": eventType,
			"event-guid": eventGUID,
		},
	)
	if counter, err := s.Metrics.WebhookCounter.GetMetricWithLabelValues(eventType); err != nil {
		l.WithError(err).Warn("Failed to get metric for eventType " + eventType)
	} else {
		counter.Inc()
	}
	// https://confluence.atlassian.com/bitbucketserver/event-payload-938025882.html
	switch eventType {
	case "repo:refs_changed":
		// Pushes, branch created or deleted, tag created or deleted
		return nil
	case "repo:comment:added":
		// Commit comments are added in this repository.
		return nil
	case "repo:comment:deleted":
		// Commit comments are deleted in this repository.
		return nil
	case "repo:modified":
		// This repository is renamed or moved.
		return nil
	case "repo:comment:edited":
		// Commit comments are edited in this repository.
		return nil
	case "repo:forked":
		// When this repository is forked.
		return nil
	case "pr:comment:deleted":
		// A pull request has comments deleted.
		return nil
	case "pr:merged":
		// A pull request is merged.
		var c stash.PullRequestEvent
		if err := json.Unmarshal(payload, &c); err != nil {
			return err
		}
		s.wg.Add(1)
		s.handlePullRequestEvent(l, c)
		return nil
	case "pr:comment:edited":
		// A pull request has comments edited.
		return nil
	case "pr:reviewer:unapproved":
		// A pull request is unapproved by a reviewer.
		return nil
	case "pr:deleted":
		// A pull request is deleted.
		return nil
	case "pr:reviewer:needs_work":
		// A pull request is marked as needs work by a reviewer.
		return nil
	case "pr:comment:added":
		// A pull request has comments added.
		var c stash.PullRequestCommentEvent
		if err := json.Unmarshal(payload, &c); err != nil {
			return err
		}
		s.wg.Add(1)
		s.handlePullRequestCommentEvent(l, c)
	case "pr:reviewer:approved":
		// A pull request is marked as approved by a reviewer.
		return nil
	case "pr:opened":
		// A pull request is opened or reopened.
		var e stash.PullRequestEvent
		if err := json.Unmarshal(payload, &e); err != nil {
			return err
		}
		s.wg.Add(1)
		s.handlePullRequestEvent(l, e)
	case "pr:reviewer:updated":
		// A pull request's reviewers have been added or removed.
		return nil
	case "pr:modified":
		// A pull request's description, title, or target branch is changed.
		var e stash.PullRequestModifiedEvent
		if err := json.Unmarshal(payload, &e); err != nil {
			return err
		}
		s.wg.Add(1)
		s.handlePullRequestModifiedEvent(l, e)
	case "pr:declined":
		// A pull request is declined.
		return nil
	}
	return nil
}
