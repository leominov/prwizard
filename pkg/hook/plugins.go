package hook

import (
	_ "github.com/leominov/prwizard/pkg/plugins/approve"
	_ "github.com/leominov/prwizard/pkg/plugins/assign"
	_ "github.com/leominov/prwizard/pkg/plugins/checklist"
	_ "github.com/leominov/prwizard/pkg/plugins/hold"
	_ "github.com/leominov/prwizard/pkg/plugins/mergedwithluv"
	_ "github.com/leominov/prwizard/pkg/plugins/prtemplate"
	_ "github.com/leominov/prwizard/pkg/plugins/review"

	// _ "github.com/leominov/prwizard/pkg/plugins/size"
	_ "github.com/leominov/prwizard/pkg/plugins/slap"
)
