package hook

import (
	"github.com/leominov/prwizard/pkg/plugins"
	"github.com/leominov/prwizard/pkg/stash"
	"github.com/sirupsen/logrus"
)

func (s *Server) handlePullRequestEvent(l *logrus.Entry, pr stash.PullRequestEvent) {
	defer s.wg.Done()
	l = l.WithFields(logrus.Fields{
		"project": pr.PullRequest.ToRef.Repository.Project.Key,
		"repo":    pr.PullRequest.ToRef.Repository.Slug,
		"pr":      pr.PullRequest.ID,
		"author":  pr.PullRequest.Author.User.Name,
	})
	l.Infof("Pull request %s.", pr.EventKey)
	for p, h := range s.Plugins.PullRequestHandlers() {
		s.wg.Add(1)
		go func(p string, h plugins.PullRequestHandler) {
			defer s.wg.Done()
			pc := s.Plugins.PluginClient
			pc.Logger = l.WithField("plugin", p)
			pc.PluginConfig = s.Plugins.Config()
			if err := h(pc, pr); err != nil {
				pc.Logger.WithError(err).Error("Error handling PullRequestEvent.")
			}
		}(p, h)
	}
}

func (s *Server) handlePullRequestModifiedEvent(l *logrus.Entry, prm stash.PullRequestModifiedEvent) {
	defer s.wg.Done()
	pr := stash.PullRequestEvent{
		EventKey:    prm.EventKey,
		Actor:       prm.Actor,
		PullRequest: prm.PullRequest,
	}
	l = l.WithFields(logrus.Fields{
		"project": pr.PullRequest.ToRef.Repository.Project.Key,
		"repo":    pr.PullRequest.ToRef.Repository.Slug,
		"pr":      pr.PullRequest.ID,
		"author":  pr.PullRequest.Author.User.Slug,
	})
	l.Infof("Pull request %s.", pr.EventKey)
	for p, h := range s.Plugins.PullRequestHandlers() {
		s.wg.Add(1)
		go func(p string, h plugins.PullRequestHandler) {
			defer s.wg.Done()
			pc := s.Plugins.PluginClient
			pc.Logger = l.WithField("plugin", p)
			pc.PluginConfig = s.Plugins.Config()
			if err := h(pc, pr); err != nil {
				pc.Logger.WithError(err).Error("Error handling PullRequestEvent.")
			}
		}(p, h)
	}
}

func (s *Server) handlePullRequestCommentEvent(l *logrus.Entry, prc stash.PullRequestCommentEvent) {
	l = l.WithFields(logrus.Fields{
		"project":   prc.PullRequest.ToRef.Repository.Project.Key,
		"repo":      prc.PullRequest.ToRef.Repository.Slug,
		"pr":        prc.PullRequest.ID,
		"author":    prc.PullRequest.Author.User.Slug,
		"comment":   prc.Comment.ID,
		"commenter": prc.Comment.Author.Slug,
	})
	l.Infof("Pull request comment %s.", prc.EventKey)
	for p, h := range s.Plugins.PullRequestCommentHandlers() {
		s.wg.Add(1)
		go func(p string, h plugins.PullRequestCommentHandler) {
			defer s.wg.Done()
			pc := s.Plugins.PluginClient
			pc.Logger = l.WithField("plugin", p)
			pc.PluginConfig = s.Plugins.Config()
			if err := h(pc, prc); err != nil {
				pc.Logger.WithError(err).Error("Error handling PullRequestCommentEvent.")
			}
		}(p, h)
	}
}
