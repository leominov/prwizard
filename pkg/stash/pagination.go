package stash

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

type Pagination struct {
	Start         int  `json:"start"`
	Size          int  `json:"size"`
	Limit         int  `json:"limit"`
	IsLastPage    bool `json:"isLastPage"`
	NextPageStart int  `json:"nextPageStart"`
}

func (c *Client) readPaginatedResults(path string, newObj func() interface{}, accumulate func(interface{}) (int, bool)) error {
	values := url.Values{
		"limit": []string{"25"},
	}
	return c.readPaginatedResultsWithValues(path, values, newObj, accumulate)
}

func (c *Client) readPaginatedResultsWithValues(path string, values url.Values, newObj func() interface{}, accumulate func(interface{}) (int, bool)) error {
	pagedURL, err := url.Parse(path)
	if err != nil {
		return err
	}
	if len(values) > 0 {
		pagedURL.RawQuery = values.Encode()
	}
	for {
		resp, err := c.doRequest(http.MethodGet, pagedURL.String(), "", nil)
		if err != nil {
			return err
		}

		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			return fmt.Errorf("return code not 2XX: %s", resp.Status)
		}

		b, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			return err
		}

		obj := newObj()
		if err := json.Unmarshal(b, obj); err != nil {
			return err
		}

		nextPageStart, ok := accumulate(obj)
		if !ok {
			break
		}

		q := pagedURL.Query()
		q.Set("start", strconv.Itoa(nextPageStart))
		pagedURL.RawQuery = q.Encode()
	}
	return nil
}
