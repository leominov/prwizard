package stash

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"strings"
)

// ValidatePayload ensures that the request payload signature matches the key.
func ValidatePayload(payload []byte, sig string, key []byte) bool {
	if !strings.HasPrefix(sig, "sha256=") {
		return false
	}
	sig = sig[7:]
	sb, err := hex.DecodeString(sig)
	if err != nil {
		return false
	}
	mac := hmac.New(sha256.New, key)
	mac.Write(payload)
	expected := mac.Sum(nil)
	return hmac.Equal(sb, expected)
}

// PayloadSignature returns the signature that matches the payload.
func PayloadSignature(payload, key []byte) string {
	mac := hmac.New(sha256.New, key)
	mac.Write(payload)
	sum := mac.Sum(nil)
	return "sha256=" + hex.EncodeToString(sum)
}
