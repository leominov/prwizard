package stash

import (
	"fmt"
	"time"
)

type PullRequestEvent struct {
	EventKey    string      `json:"eventKey"`
	Actor       Actor       `json:"actor"`
	PullRequest PullRequest `json:"pullRequest"`
}

type PullRequestModifiedEvent struct {
	PullRequestEvent
	PreviousTitle       string `json:"previousTitle"`
	PreviousDescription string `json:"previousDescription"`
	PreviousTarget      Target `json:"previousTarget"`
}

type PullRequestCommentEvent struct {
	EventKey        string      `json:"eventKey"`
	Actor           Actor       `json:"actor"`
	PullRequest     PullRequest `json:"pullRequest"`
	Comment         Comment     `json:"comment"`
	CommentParentID int         `json:"commentParentId"`
}

type Comment struct {
	ID          int    `json:"id"`
	Version     int    `json:"version"`
	Text        string `json:"text"`
	Author      Actor  `json:"author"`
	CreatedDate int    `json:"createdDate"`
	UpdatedDate int    `json:"updatedDate"`
}

func (c *Comment) GetUpdatedDate() time.Time {
	return time.Unix(int64(c.UpdatedDate)/int64(1000), 0)
}

func (c *Comment) GetCreatedDate() time.Time {
	return time.Unix(int64(c.CreatedDate)/int64(1000), 0)
}

type Actor struct {
	Name        string `json:"name"`
	Email       string `json:"emailAddress"`
	ID          int    `json:"id"`
	DisplayName string `json:"displayName"`
	Active      bool   `json:"active"`
	Slug        string `json:"slug"`
	Type        string `json:"type"`
}

type Repository struct {
	ID            int     `json:"id"`
	Slug          string  `json:"slug"`
	Name          string  `json:"name"`
	ScmID         string  `json:"scmId"`
	State         string  `json:"state"`
	StatusMessage string  `json:"statusMessage"`
	Forkable      bool    `json:"forkable"`
	Public        bool    `json:"public"`
	Project       Project `json:"project"`
}

type Project struct {
	ID     int    `json:"id"`
	Key    string `json:"key"`
	Name   string `json:"name"`
	Public bool   `json:"public"`
	Type   string `json:"type"`
}

type Author struct {
	User     Actor  `json:"user"`
	Role     string `json:"role"`
	Approved bool   `json:"approved"`
	Status   string `json:"status"`
}

type Target struct {
	ID              string `json:"id"`
	DisplayID       string `json:"displayId"`
	LatestCommit    string `json:"latestCommit"`
	LatestChangeset string `json:"latestChangeset"`
}

type Ref struct {
	ID           string     `json:"id"`
	DisplayID    string     `json:"displayId"`
	LatestCommit string     `json:"latestCommit"`
	Repository   Repository `json:"repository"`
}

type PullRequest struct {
	ID           int       `json:"id"`
	Version      int       `json:"version"`
	Title        string    `json:"title"`
	Description  string    `json:"description"`
	State        string    `json:"state"`
	Open         bool      `json:"open"`
	Closed       bool      `json:"closed"`
	CreatedDate  int       `json:"createdDate"`
	UpdatedDate  int       `json:"updatedDate"`
	FromRef      *Ref      `json:"fromRef,omitempty"`
	ToRef        *Ref      `json:"toRef,omitempty"`
	Locked       bool      `json:"locked"`
	Author       *Author   `json:"author,omitempty"`
	Reviewers    []*Author `json:"reviewers,omitempty"`
	Participants []*Author `json:"participants,omitempty"`
}

func (p *PullRequest) GetAllParticipants() []*Author {
	var resultList []*Author
	users := make(map[string]bool)
	for _, user := range p.Participants {
		_, ok := users[user.User.Slug]
		if !ok {
			resultList = append(resultList, user)
		}
	}
	for _, user := range p.Reviewers {
		_, ok := users[user.User.Slug]
		if !ok {
			resultList = append(resultList, user)
		}
	}
	return resultList
}

func (p *PullRequest) IsUserInParticipants(user string) bool {
	users := p.GetAllParticipants()
	for _, u := range users {
		if user == u.User.Slug {
			return true
		}
	}
	return false
}

func (p *PullRequest) Path() string {
	result := fmt.Sprintf(
		"/projects/%s/repos/%s/pull-requests/%d/overview",
		p.ToRef.Repository.Project.Key,
		p.ToRef.Repository.Slug,
		p.ID,
	)
	return result
}

func (p *PullRequest) GetUpdatedDate() time.Time {
	return time.Unix(int64(p.UpdatedDate)/int64(1000), 0)
}

func (p *PullRequest) GetCreatedDate() time.Time {
	return time.Unix(int64(p.CreatedDate)/int64(1000), 0)
}

type Path struct {
	Components []string `json:"components"`
	Parent     string   `json:"parent"`
	Name       string   `json:"name"`
	ToString   string   `json:"toString"`
}

type Change struct {
	Path             *Path
	ContentID        string `json:"contentId"`
	FromContentID    string `json:"fromContentId"`
	Executable       bool   `json:"executable"`
	PercentUnchanged int    `json:"percentUnchanged"`
	Type             string `json:"type"`
	NodeType         string `json:"nodeType"`
	SrcExecutable    bool   `json:"srcExecutable"`
}

type ChangesResponse struct {
	Pagination
	FromHash string    `json:"fromHash"`
	ToHash   string    `json:"toHash"`
	Changes  []*Change `json:"values"`
}

type PullRequestUser struct {
	Name string `json:"name"`
}

type PullRequestReviewer struct {
	User     PullRequestUser `json:"user"`
	Role     string          `json:"role"`
	Status   string          `json:"status"`
	Approved bool            `json:"approved"`
}

type PullRequestComment struct {
	Text string `json:"text"`
}

type PullRequestCommentResponse struct {
	ID      int    `json:"id"`
	Version int    `json:"version"`
	Text    string `json:"text"`
}

type PullRequestReplyComment struct {
	PullRequestComment
	Parent PullRequestCommentParent `json:"parent"`
}

type PullRequestCommentParent struct {
	ID string `json:"id"`
}

type PullRequestDiff struct {
	FromHash string                  `json:"fromHash"`
	ToHash   string                  `json:"toHash"`
	Diffs    []*PullRequestDiffEntry `json:"diffs"`
}

type PullRequestDiffEntry struct {
	Source      *PullRequestDiffLocation `json:"source"`
	Destination *PullRequestDiffLocation `json:"destination"`
	Hunks       []*Hunk                  `json:"hunks"`
	Truncated   bool                     `json:"truncated"`
}

type Hunk struct {
	Context         string     `json:"context"`
	SourceLine      int        `json:"sourceLine"`
	SourceSpan      int        `json:"sourceSpan"`
	DestinationLine int        `json:"destinationLine"`
	DestinationSpan int        `json:"destinationSpan"`
	Segments        []*Segment `json:"segments"`
	Truncated       bool       `json:"truncated"`
}

const (
	SegmentContext = "CONTEXT"
	SegmentAdded   = "ADDED"
	SegmtntRemoved = "REMOVED"
)

type Segment struct {
	Type      string  `json:"type"`
	Lines     []*Line `json:"lines"`
	Truncated bool    `json:"truncated"`
}

type Line struct {
	Source      int    `json:"source"`
	Destination int    `json:"destination"`
	Line        string `json:"line"`
	Truncated   bool   `json:"truncated"`
}

type PullRequestDiffLocation struct {
	Components []string `json:"components"`
	Parent     string   `json:"parent"`
	Name       string   `json:"name"`
	Extension  string   `json:"extension"`
	ToString   string   `json:"toString"`
}

type TaskAnchor struct {
	ID   int    `json:"id"`
	Type string `json:"type"`
}

type Task struct {
	Anchor        TaskAnchor `json:"anchor"`
	PullRequestID int        `json:"pullRequestId"`
	RepositoryID  int        `json:"repositoryId"`
	Text          string     `json:"text"`
	State         string     `json:"state"`
	PendingSync   bool       `json:"pendingSync"`
}

type WebhookConfiguration struct {
	Secret string `json:"secret"`
}

type Webhook struct {
	ID            int                   `json:"id"`
	Name          string                `json:"name"`
	CreatedDate   int                   `json:"createdDate"`
	UpdatedDate   int                   `json:"updatedDate"`
	Events        []string              `json:"events"`
	Configuration *WebhookConfiguration `json:"configuration"`
	URL           string                `json:"url"`
	Active        bool                  `json:"active"`
}

func (w *Webhook) SameAs(wh *Webhook) bool {
	return w.URL == wh.URL
}

type ProjectsResponse struct {
	Pagination
	Projects []*Project `json:"values"`
}

type ReposResponse struct {
	Pagination
	Repos []*Repository `json:"values"`
}

type WebhooksResponse struct {
	Pagination
	Webhooks []*Webhook `json:"values"`
}

type CommentsResponse struct {
	Pagination
	Comments []*Comment `json:"values"`
}

type PullRequestsResponse struct {
	Pagination
	PullRequests []*PullRequest `json:"values"`
}

type Settings struct {
	RequiredAllApprovers       bool           `json:"requiredAllApprovers"`
	RequiredAllTasksComplete   bool           `json:"requiredAllTasksComplete"`
	RequiredApprovers          int            `json:"requiredApprovers"`
	RequiredSuccessfulBuilds   int            `json:"requiredSuccessfulBuilds"`
	RequiredApproversMergeHook BoolNumSetting `json:"com.atlassian.bitbucket.server.bundled-hooks.requiredApproversMergeHook"`
	RequiredBuildsMergeCheck   BoolNumSetting `json:"com.atlassian.bitbucket.server.bitbucket-build.requiredBuildsMergeCheck"`
}

type BoolNumSetting struct {
	Enabled bool `json:"enabled"`
	Count   int  `json:"count"`
}

type TagsResponse struct {
	Pagination
	Tags []*Tag `json:"values"`
}

type Tag struct {
	ID               string `json:"id"`
	DisplayID        string `json:"displayId"`
	Type             string `json:"type"`
	LatestCommit     string `json:"latestCommit"`
	LattestChangeset string `json:"latestChangeset"`
	Hash             string `json:"hash"`
}

type Activity struct {
	ID            int      `json:"id"`
	User          Actor    `json:"user"`
	CreatedDate   int      `json:"createdDate"`
	Action        string   `json:"action"`
	CommentAction string   `json:"commentAction"`
	Comment       *Comment `json:"comment"`
}

func (a *Activity) GetCreatedDate() time.Time {
	return time.Unix(int64(a.CreatedDate)/int64(1000), 0)
}

type ActivitiesResponse struct {
	Pagination
	Activities []*Activity `json:"values"`
}

type BuildState string

const (
	InprogressBuildState BuildState = "INPROGRESS"
	SuccessfulBuildState BuildState = "SUCCESSFUL"
	FailedBuildState     BuildState = "FAILED"
)

type BuildStatus struct {
	State       BuildState `json:"state"`
	Key         string     `json:"key"`
	Name        string     `json:"name"`
	URL         string     `json:"url"`
	Description string     `json:"description"`
}
