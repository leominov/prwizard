// https://developer.atlassian.com/docs/
// https://docs.atlassian.com/bitbucket-server/rest/5.11.1/bitbucket-rest.html
// https://docs.atlassian.com/bitbucket-server/rest/5.11.1/bitbucket-jira-rest.html
// https://docs.atlassian.com/bitbucket-server/rest/5.11.1/bitbucket-rest.html#paging-params
// https://docs.atlassian.com/bitbucket-server/rest/5.11.1/bitbucket-rest.html#errors-and-validation
// https://docs.atlassian.com/bitbucket-server/rest/4.11.1/bitbucket-rest.html#idm21616
// https://confluence.atlassian.com/bitbucketserver/event-payload-938025882.html?utm_campaign=in-app-help&utm_medium=in-app-help&utm_source=stash#Eventpayload-repositoryevents
// https://developer.atlassian.com/server/bitbucket/how-tos/updating-build-status-for-commits/

package stash

import (
	"encoding/json"
	"net"
	"net/http"
	"time"

	"bytes"
	"fmt"
	"io"
	"strings"

	"io/ioutil"

	"github.com/leominov/prwizard/pkg/rand"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var (
	BuildStatusKey  = "PRWizard"
	BuildStatusName = "PRWizard"
	BuildStatusURL  = "http://github.com/leominov/prwizard"

	// ErrFileNotFound if file not found
	ErrFileNotFound = errors.New("File not found")

	defaultTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}
	defaultHttpClient = &http.Client{
		Timeout:   time.Second * 10,
		Transport: defaultTransport,
	}
)

// Client is Stash client
type Client struct {
	logger     *logrus.Entry
	endpoint   string
	authSecret string
	client     *http.Client
	botName    string
}

func init() {
	rand.SeedMathRand()
}

// NewClient returns Stash client
func NewClient(endpoint string, httpCli *http.Client) *Client {
	endpoint = strings.TrimRight(endpoint, "/")
	if httpCli == nil {
		httpCli = defaultHttpClient
	}
	return &Client{
		logger:   logrus.WithField("client", "stash"),
		client:   httpCli,
		endpoint: endpoint,
	}
}

func (c *Client) log(methodName string, args ...interface{}) {
	if c.logger == nil {
		return
	}
	var as []string
	for _, arg := range args {
		as = append(as, fmt.Sprintf("%v", arg))
	}
	c.logger.Infof("%s(%s)", methodName, strings.Join(as, ", "))
}

func (c *Client) doRequest(method, path, accept string, body interface{}) (*http.Response, error) {
	var buf io.Reader
	if body != nil {
		b, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}
		buf = bytes.NewBuffer(b)
	}
	req, err := http.NewRequest(method, path, buf)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", c.authSecret)
	req.Header.Add("Content-Type", "application/json")
	if accept != "" {
		req.Header.Add("Accept", accept)
	}
	req.Close = true
	return c.client.Do(req)
}

func (c *Client) getUserData() error {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/plugins/servlet/applinks/whoami", c.endpoint), nil)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", c.authSecret)
	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("incorrect response code: %s", resp.Status)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	c.botName = string(bytes.TrimSpace(b))
	return nil
}

// Endpoint returns Stash endpoint
func (c *Client) Endpoint() string {
	return c.endpoint
}

// BotName returns Bot name or error. Useful for filtering incoming events.
func (c *Client) BotName() (string, error) {
	if c.botName == "" {
		if err := c.getUserData(); err != nil {
			return "", fmt.Errorf("fetching bot name from Stash: %v", err)
		}
	}
	return c.botName, nil
}

// SetCredentials to set up auth token
func (c *Client) SetCredentials(authSecret string) {
	c.authSecret = authSecret
}

// ReplyComment reply to another comment in pull-request
func (c *Client) ReplyComment(project, repo string, prID int, commID, comment string) error {
	c.log("ReplyComment", project, repo, prID, commID, comment)
	pc := PullRequestReplyComment{}
	pc.Text = comment
	pc.Parent.ID = commID
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/comments",
		c.endpoint,
		project,
		repo,
		prID,
	)
	resp, err := c.doRequest(
		http.MethodPost,
		url,
		"",
		&pc,
	)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

// DeleteReviewer - Unassigns a participant from the REVIEWER role they may have
// been given in a pull request.
// https://docs.atlassian.com/bitbucket-server/rest/5.11.1/bitbucket-rest.html#idm46358292605584
func (c *Client) DeleteReviewer(project, repo string, prID int, reviewer string) error {
	c.log("DeleteReviewer", project, repo, prID, reviewer)
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/participants/%s",
		c.endpoint,
		project,
		repo,
		prID,
		reviewer,
	)
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", c.authSecret)
	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

// AddReviewer assigns a participant to an explicit role in pull request.
// Currently only the REVIEWER role may be assigned.
// https://docs.atlassian.com/bitbucket-server/rest/5.11.1/bitbucket-rest.html#idm46358292634432
func (c *Client) AddReviewer(project, repo string, prID int, reviewer string) error {
	c.log("AddReviewer", project, repo, prID, reviewer)
	pr := PullRequestReviewer{
		Role: "REVIEWER",
		User: PullRequestUser{
			Name: reviewer,
		},
	}
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/participants",
		c.endpoint,
		project,
		repo,
		prID,
	)
	resp, err := c.doRequest(
		http.MethodPost,
		url,
		"",
		&pr,
	)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

// CreateComment creates comment in PR
// TODO(l.aminov): Return comment ID
func (c *Client) CreateComment(project, repo string, prID int, comment string) (*PullRequestCommentResponse, error) {
	c.log("CreateComment", project, repo, prID, comment)
	pc := PullRequestComment{
		Text: comment,
	}
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/comments",
		c.endpoint,
		project,
		repo,
		prID,
	)
	resp, err := c.doRequest(
		http.MethodPost,
		url,
		"",
		&pc,
	)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var prComment *PullRequestCommentResponse
	err = json.Unmarshal(b, &prComment)
	if err != nil {
		return nil, err
	}
	return prComment, nil
}

// GetPullRequestDiff retrieve the diff for a specified file path between two provided revisions
func (c *Client) GetPullRequestDiff(project, repo string, prID int) (*PullRequestDiff, error) {
	c.log("GetPullRequestDiff", project, repo, prID)
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/diff?withComments=false&contextLines=0",
		c.endpoint,
		project,
		repo,
		prID,
	)
	resp, err := c.doRequest(http.MethodGet, url, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var prDiff *PullRequestDiff
	err = json.Unmarshal(b, &prDiff)
	if err != nil {
		return nil, err
	}
	return prDiff, nil
}

// GetRawFile returns []byte or error
func (c *Client) GetRawFile(project, repo, filePath, base string) ([]byte, error) {
	c.log("GetRawFile", project, repo, filePath, base)
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/projects/%s/repos/%s/browse/%s?at=%s&raw", c.endpoint, project, repo, filePath, base), nil)
	if err != nil {
		return []byte{}, err
	}
	req.Header.Set("Authorization", c.authSecret)
	resp, err := c.client.Do(req)
	if err != nil {
		return []byte{}, err
	}
	defer resp.Body.Close()
	switch resp.StatusCode {
	case http.StatusNotFound:
		return []byte{}, ErrFileNotFound
	case http.StatusOK:
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return []byte{}, err
		}
		return b, nil
	}
	return []byte{}, fmt.Errorf("incorrect response: %s", resp.Status)
}

// SetNeedsWork update reviewer with NEEDS_WORK status
func (c *Client) SetNeedsWork(project, repo string, prID int, reviewer string) error {
	return c.SetReviewerStatus(project, repo, prID, reviewer, "NEEDS_WORK", false)
}

// SetReviewerStatus change the current user's status for a pull request.
func (c *Client) SetReviewerStatus(project, repo string, prID int, reviewer, status string, approved bool) error {
	c.log("SetReviewerStatus", project, repo, prID, reviewer, status, approved)
	pr := PullRequestReviewer{
		Role:     "REVIEWER",
		Status:   status,
		Approved: approved,
		User: PullRequestUser{
			Name: reviewer,
		},
	}
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/participants/%s",
		c.endpoint,
		project,
		repo,
		prID,
		reviewer,
	)
	resp, err := c.doRequest(
		http.MethodPut,
		url,
		"",
		&pr,
	)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

// AddTask adds task to comment
func (c *Client) AddTask(repositoryID, prID, commentID int, text string) error {
	c.log("AddTask", repositoryID, prID, commentID, text)
	task := Task{
		Anchor: TaskAnchor{
			ID:   commentID,
			Type: "COMMENT",
		},
		PullRequestID: prID,
		RepositoryID:  repositoryID,
		Text:          text,
		State:         "OPEN",
		PendingSync:   true,
	}
	url := fmt.Sprintf("%s/rest/api/latest/tasks", c.endpoint)
	resp, err := c.doRequest(
		http.MethodPost,
		url,
		"",
		&task,
	)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

// PUT /rest/api/latest/projects/TEST/repos/test/pull-requests/48?avatarSize=32 HTTP/1.1
// Authorization: Basic bC5hbWlub3Y6MTIzNDU=
// Content-Type: application/json; charset=utf-8
// Host: 127.0.0.1:7990
// {"title":"[WIP] [size/XL] Change 1/2/3","reviewers":[{"user":{"name":"test2","emailAddress":"test2@paradev.ru","id":3,"displayName":"Test User #2","active":true,"slug":"test2","type":"NORMAL","links":{"self":[{"href":"http://127.0.0.1:7990/users/test2"}]}},"role":"PARTICIPANT","approved":false,"status":"UNAPPROVED"}],"toRef":{"id":"refs/heads/master"},"version":12}

// GetProject retrieve the project matching the supplied projectKey
func (c *Client) GetProject(projectKey string) (*Project, error) {
	c.log("GetProject", projectKey)
	url := fmt.Sprintf("%s/rest/api/1.0/projects/%s", c.endpoint, projectKey)
	resp, err := c.doRequest(http.MethodGet, url, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var project *Project
	err = json.Unmarshal(b, &project)
	if err != nil {
		return nil, err
	}
	return project, nil
}

// GetRepo retrieve the repository matching the supplied projectKey and repositorySlug
func (c *Client) GetRepo(projectKey, repoSlug string) (*Repository, error) {
	c.log("GetRepo", projectKey, repoSlug)
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s",
		c.endpoint,
		projectKey,
		repoSlug,
	)
	resp, err := c.doRequest(http.MethodGet, url, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var repo *Repository
	err = json.Unmarshal(b, &repo)
	if err != nil {
		return nil, err
	}
	return repo, nil
}

// GetTags retrieve the tags
func (c *Client) GetTags(projectKey, repoSlug string) ([]*Tag, error) {
	var tags []*Tag
	c.log("GetTags", projectKey, repoSlug)
	path := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/tags",
		c.endpoint,
		projectKey,
		repoSlug,
	)
	err := c.readPaginatedResults(
		path,
		func() interface{} {
			return &TagsResponse{}
		},
		func(obj interface{}) (int, bool) {
			resp := obj.(*TagsResponse)
			tags = append(tags, resp.Tags...)
			if resp.IsLastPage {
				return 0, false
			}
			return resp.NextPageStart, true
		},
	)
	return tags, err
}

// GetRepos retrieve repositories from the project corresponding to the supplied projectKey.
// GetProjects retrieve a page of projects
func (c *Client) GetRepos(projectKey string) ([]*Repository, error) {
	var repos []*Repository
	c.log("GetRepos", projectKey)
	path := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos",
		c.endpoint,
		projectKey,
	)
	err := c.readPaginatedResults(
		path,
		func() interface{} {
			return &ReposResponse{}
		},
		func(obj interface{}) (int, bool) {
			resp := obj.(*ReposResponse)
			repos = append(repos, resp.Repos...)
			if resp.IsLastPage {
				return 0, false
			}
			return resp.NextPageStart, true
		},
	)
	return repos, err
}

func (c *Client) GetAffectedFiles(projectKey, repoSlug string, prID int) ([]string, error) {
	var affectedFiles []string
	changes, err := c.GetChanges(projectKey, repoSlug, prID)
	if err != nil {
		return affectedFiles, err
	}
	for _, change := range changes {
		affectedFiles = append(affectedFiles, change.Path.ToString)
	}
	return affectedFiles, nil
}

// GetChanges retrieve a page of changes made in a specified commit
func (c *Client) GetChanges(projectKey, repoSlug string, prID int) ([]*Change, error) {
	var changes []*Change
	c.log("GetChanges", projectKey, repoSlug, prID)
	path := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/changes",
		c.endpoint,
		projectKey,
		repoSlug,
		prID,
	)
	err := c.readPaginatedResults(
		path,
		func() interface{} {
			return &ChangesResponse{}
		},
		func(obj interface{}) (int, bool) {
			resp := obj.(*ChangesResponse)
			changes = append(changes, resp.Changes...)
			if resp.IsLastPage {
				return 0, false
			}
			return resp.NextPageStart, true
		},
	)
	return changes, err
}

func (c *Client) GetPullRequest(projectKey, repoSlug string, prID int) (*PullRequest, error) {
	c.log("GetPullRequest", projectKey, repoSlug, prID)
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d",
		c.endpoint,
		projectKey,
		repoSlug,
		prID,
	)
	resp, err := c.doRequest(http.MethodGet, url, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var pr *PullRequest
	err = json.Unmarshal(b, &pr)
	if err != nil {
		return nil, err
	}
	return pr, nil
}

// Try use channels
func (c *Client) UpdatePullRequest(projectKey, repoSlug, field string, pr PullRequest) error {
	c.log("UpdatePullRequest", projectKey, repoSlug, field, fmt.Sprintf("%+v", pr))
	path := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d",
		c.endpoint,
		projectKey,
		repoSlug,
		pr.ID,
	)
	backoff := rand.RandomStagger(time.Second * 3)
	maxTries := 3
	for try := 0; try < maxTries; try++ {
		pr.Author = nil
		pr.Participants = nil
		resp, err := c.doRequest(http.MethodPut, path, "", &pr)
		if err != nil {
			return err
		}
		resp.Body.Close()
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			prUpdated, err := c.GetPullRequest(projectKey, repoSlug, pr.ID)
			if err != nil {
				return err
			}
			switch field {
			case "title":
				prUpdated.Title = pr.Title
			case "description":
				prUpdated.Description = pr.Description
			default:
				return nil
			}
			pr = *prUpdated
			if try+1 < maxTries {
				time.Sleep(backoff)
				backoff *= 2
			}
		}
	}
	return fmt.Errorf("reached maximum number of retries (%d) checking mergeability", maxTries)
}

// GetActivities find activities in pull request
func (c *Client) GetActivities(projectKey, repoSlug string, prID int) ([]*Activity, error) {
	var activities []*Activity
	c.log("GetActivities", projectKey, repoSlug, prID)
	path := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/activities",
		c.endpoint,
		projectKey,
		repoSlug,
		prID,
	)
	err := c.readPaginatedResults(
		path,
		func() interface{} {
			return &ActivitiesResponse{}
		},
		func(obj interface{}) (int, bool) {
			resp := obj.(*ActivitiesResponse)
			activities = append(activities, resp.Activities...)
			if resp.IsLastPage {
				return 0, false
			}
			return resp.NextPageStart, true
		},
	)
	return activities, err
}

// GetComments find coments in this pull request by path
func (c *Client) GetComments(projectKey, repoSlug string, prID int, filepath string) ([]*Comment, error) {
	var comments []*Comment
	c.log("GetComments", projectKey, repoSlug, prID, filepath)
	path := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/comments?path=%s",
		c.endpoint,
		projectKey,
		repoSlug,
		prID,
		filepath,
	)
	err := c.readPaginatedResults(
		path,
		func() interface{} {
			return &CommentsResponse{}
		},
		func(obj interface{}) (int, bool) {
			resp := obj.(*CommentsResponse)
			comments = append(comments, resp.Comments...)
			if resp.IsLastPage {
				return 0, false
			}
			return resp.NextPageStart, true
		},
	)
	return comments, err
}

// GetPullRequests find pull requests in this repository
func (c *Client) GetPullRequests(projectKey, repoSlug string) ([]*PullRequest, error) {
	var pullRequests []*PullRequest
	c.log("GetPullRequests", projectKey, repoSlug)
	path := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests",
		c.endpoint,
		projectKey,
		repoSlug,
	)
	err := c.readPaginatedResults(
		path,
		func() interface{} {
			return &PullRequestsResponse{}
		},
		func(obj interface{}) (int, bool) {
			resp := obj.(*PullRequestsResponse)
			pullRequests = append(pullRequests, resp.PullRequests...)
			if resp.IsLastPage {
				return 0, false
			}
			return resp.NextPageStart, true
		},
	)
	return pullRequests, err
}

// GetWebhooks find webhooks in this repository
func (c *Client) GetWebhooks(projectKey, repoSlug string) ([]*Webhook, error) {
	var webhooks []*Webhook
	c.log("GetWebhooks", projectKey, repoSlug)
	path := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/webhooks",
		c.endpoint,
		projectKey,
		repoSlug,
	)
	err := c.readPaginatedResults(
		path,
		func() interface{} {
			return &WebhooksResponse{}
		},
		func(obj interface{}) (int, bool) {
			resp := obj.(*WebhooksResponse)
			webhooks = append(webhooks, resp.Webhooks...)
			if resp.IsLastPage {
				return 0, false
			}
			return resp.NextPageStart, true
		},
	)
	return webhooks, err
}

// GetWebhook get a webhook by id
func (c *Client) GetWebhook(projectKey, repoSlug string, id int) (*Webhook, error) {
	c.log("GetWebhook", projectKey, repoSlug, id)
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/webhooks/%d",
		c.endpoint,
		projectKey,
		repoSlug,
		id,
	)
	resp, err := c.doRequest(http.MethodGet, url, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var webhook *Webhook
	err = json.Unmarshal(b, &webhook)
	if err != nil {
		return nil, err
	}
	return webhook, nil
}

// DeleteWebhook delete a webhook for the repository specified via the URL
func (c *Client) DeleteWebhook(projectKey, repoSlug string, id int) error {
	c.log("DeleteWebhook", projectKey, repoSlug, id)
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/webhooks/%d",
		c.endpoint,
		projectKey,
		repoSlug,
		id,
	)
	resp, err := c.doRequest(http.MethodDelete, url, "", nil)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

// AddWebhook create a webhook for the repository specified via the URL
func (c *Client) AddWebhook(projectKey, repoSlug string, webhook *Webhook) error {
	c.log("AddWebhook", projectKey, repoSlug, fmt.Sprintf("%+v", webhook))
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/webhooks",
		c.endpoint,
		projectKey,
		repoSlug,
	)
	resp, err := c.doRequest(http.MethodPost, url, "", &webhook)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

func (c *Client) SwitchWebhookActive(projectKey, repoSlug string, id int, active bool) error {
	c.log("SwitchWebhookActive", projectKey, repoSlug, id, active)
	webhook, err := c.GetWebhook(projectKey, repoSlug, id)
	if err != nil {
		return err
	}
	if webhook.Active == active {
		return nil
	}
	webhook.Active = active
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/webhooks/%d",
		c.endpoint,
		projectKey,
		repoSlug,
		id,
	)
	resp, err := c.doRequest(http.MethodPut, url, "", &webhook)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

// GetRepoPRSettings retrieve the pull request settings for the context repository
func (c *Client) GetRepoPRSettings(projectKey, repoSlug string) (*Settings, error) {
	c.log("GetRepoPRSettings", projectKey, repoSlug)
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/settings/pull-requests",
		c.endpoint,
		projectKey,
		repoSlug,
	)
	resp, err := c.doRequest(http.MethodGet, url, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var settings *Settings
	err = json.Unmarshal(b, &settings)
	if err != nil {
		return nil, err
	}
	return settings, nil
}

// TODO(l.aminov): Move to user lib
func (c *Client) DeleteWebhookDuplicates(projectKey, repoSlug string, webhook *Webhook) error {
	var uniqueWebhookID int
	webhooks, err := c.GetWebhooks(projectKey, repoSlug)
	if err != nil {
		return err
	}
	for _, wh := range webhooks {
		if !wh.SameAs(webhook) {
			continue
		}
		if uniqueWebhookID == 0 {
			uniqueWebhookID = wh.ID
			continue
		}
		err := c.DeleteWebhook(projectKey, repoSlug, wh.ID)
		if err != nil {
			return err
		}
	}
	return nil
}

// TODO(l.aminov): Move to user lib
func (c *Client) CheckWebhookExists(projectKey, repoSlug string, webhook *Webhook) (bool, error) {
	webhooks, err := c.GetWebhooks(projectKey, repoSlug)
	if err != nil {
		return false, err
	}
	for _, wh := range webhooks {
		if wh.SameAs(webhook) {
			return true, nil
		}
	}
	return false, nil
}

// GetProjects retrieve a page of projects
func (c *Client) GetProjects() ([]*Project, error) {
	var projects []*Project
	c.log("GetProjects")
	path := fmt.Sprintf("%s/rest/api/1.0/projects", c.endpoint)
	err := c.readPaginatedResults(
		path,
		func() interface{} {
			return &ProjectsResponse{}
		},
		func(obj interface{}) (int, bool) {
			resp := obj.(*ProjectsResponse)
			projects = append(projects, resp.Projects...)
			if resp.IsLastPage {
				return 0, false
			}
			return resp.NextPageStart, true
		},
	)
	return projects, err
}

func (c *Client) GetLatestTag(projectKey, repoSlug string) (*Tag, error) {
	c.log("GetLatestTag", projectKey, repoSlug)
	path := fmt.Sprintf("%s/rest/api/1.0/projects/%s/repos/%s/tags?limit=1", c.endpoint, projectKey, repoSlug)
	resp, err := c.doRequest(http.MethodGet, path, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var tagsResp *TagsResponse
	err = json.Unmarshal(b, &tagsResp)
	if err != nil {
		return nil, err
	}
	if len(tagsResp.Tags) == 0 {
		return nil, errors.New("Empty tags list")
	}
	return tagsResp.Tags[0], nil
}

// DeclinePullRequest decline pull request
func (c *Client) DeclinePullRequest(projectKey, repoSlug string, prID int, version int) error {
	c.log("DeclinePullRequest", projectKey, repoSlug, prID, version)
	url := fmt.Sprintf(
		"%s/rest/api/1.0/projects/%s/repos/%s/pull-requests/%d/decline?version=%d",
		c.endpoint,
		projectKey,
		repoSlug,
		prID,
		version,
	)
	resp, err := c.doRequest(http.MethodPost, url, "", nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Unexpected response: %s", string(b))
	}
	return nil
}

// SetBuildStatus set comming build status
func (c *Client) SetBuildStatus(revision string, state BuildState, descr string) error {
	c.log("SetBuildStatus", revision, state)
	buildStatus := BuildStatus{
		State:       state,
		Key:         BuildStatusKey,
		Name:        BuildStatusName,
		URL:         BuildStatusURL,
		Description: descr,
	}
	url := fmt.Sprintf(
		"%s/rest/build-status/1.0/commits/%s",
		c.endpoint,
		revision,
	)
	resp, err := c.doRequest(http.MethodPost, url, "", &buildStatus)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}
