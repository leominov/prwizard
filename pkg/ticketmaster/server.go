package ticketmaster

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/leominov/prwizard/pkg/stash"
	"github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"github.com/leominov/prwizard/pkg/jira"
	badge "github.com/narqo/go-badge"
)

const (
	badgeTemplateText = "open %s issues"
)

type Server struct {
	JiraClient  *jira.Client
	StashClient *stash.Client
}

func issueCountToColor(total int) badge.Color {
	if total == 0 {
		return badge.ColorBrightgreen
	}
	if total <= 10 {
		return badge.ColorYellow
	}
	if total <= 20 {
		return badge.ColorOrange
	}
	return badge.ColorRed
}

// /secure/ShowConstantsHelp.jspa?decorator=popup#StatusTypes
func issueStatusToColor(status string) badge.Color {
	switch strings.ToLower(status) {
	case "new":
	case "reopened":
	case "hold":
	case "to do":
	case "todo":
	case "backlog":
	case "open":
		return badge.ColorBlue
	case "in progress":
	case "in review":
	case "assigned":
	case "testing":
	case "developing":
	case "review":
	case "design":
	case "staged":
	case "analysis":
	case "waiting approval":
	case "waiting results":
	case "ready to launch":
	case "launched":
	case "signing":
	case "deploy":
	case "ready to develop":
	case "ready for testing":
	case "ready fo analysis":
	case "backup":
	case "monitoring":
	case "planning":
	case "waiting":
	case "docimentation":
	case "ready to test":
		return badge.ColorOrange
	}
	return badge.ColorBrightgreen
}

func (s *Server) NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/svg+xml")
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")

	badge.Render("not found", "0", badge.ColorRed, w)
}

func (s *Server) UnresolvedIssuesByLabelSVGHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/svg+xml")
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")

	vars := mux.Vars(r)
	label := vars["label"]
	total, err := s.JiraClient.CountUnresolvedIssuesByLabel(label)
	if err != nil {
		logrus.Error(err)
		badge.Render(fmt.Sprintf(badgeTemplateText, label), "N/A", badge.ColorGrey, w)
		return
	}
	badge.Render(fmt.Sprintf(badgeTemplateText, label), strconv.Itoa(total), issueCountToColor(total), w)
}

func (s *Server) IssueStatusSVGHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/svg+xml")
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")

	vars := mux.Vars(r)
	key := vars["key"]
	issue, err := s.JiraClient.GetIssue(key)
	if err != nil {
		logrus.Error(err)
		badge.Render(key, "N/A", badge.ColorGrey, w)
		return
	}
	badge.Render(key, strings.ToUpper(issue.Fileds.Status.Name), issueStatusToColor(issue.Fileds.Status.Name), w)
}

func (s *Server) LatestTagSVGHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/svg+xml")
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")

	vars := mux.Vars(r)
	projectKey := vars["projectKey"]
	repoSlug := vars["repoSlug"]

	tag, err := s.StashClient.GetLatestTag(projectKey, repoSlug)
	if err != nil {
		logrus.Error(err)
		badge.Render("tag", "N/A", badge.ColorGrey, w)
		return
	}
	badge.Render("tag", tag.DisplayID, badge.ColorBrightgreen, w)
}
