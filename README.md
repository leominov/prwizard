# PRWizard aka Atlas

* prwizard
* grower
* ticketmaster
* watcher
* pullreminders

## Development environment

* [Atlassian SDK](https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/)
* `atlas-run-standalone --product bitbucket --rest-version 5.11.1 --version 6.2.0`
* Sign-in as `admin:admin`
* `go run ./cmd/prwizard/ --stash-auth-file etc/stash/auth --hmac-secret-file etc/webhook/hmac -stash-endpoint http://127.0.0.1:7990/bitbucket`
