module github.com/leominov/prwizard

go 1.14

require (
	github.com/cloudfoundry/cli v6.36.2+incompatible
	github.com/dustin/go-humanize v1.0.0
	github.com/ghodss/yaml v1.0.0
	github.com/golang/freetype v0.0.0-20160221112527-9ce4eec92a4b // indirect
	github.com/gorilla/mux v1.7.3
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/narqo/go-badge v0.0.0-20160308224023-3014a17b062a
	github.com/onsi/ginkgo v1.12.3 // indirect
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v1.1.0
	github.com/prometheus/client_model v0.0.0-20190812154241-14fe0d1b01d4 // indirect
	github.com/prometheus/procfs v0.0.4 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	golang.org/x/image v0.0.0-20160107224602-7c492694a644 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
