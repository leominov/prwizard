GIT_COMMIT := $(shell git rev-parse HEAD)

.PHONY: run
run:
	@go run cmd/prwizard/main.go --slack-token-file .slack --stash-auth-file .auth --hmac-secret-file .hmac

.PHONY: linux
linux: linux-prwizard linux-ticketmaster linux-watcher

.PHONY: linux-prwizard
linux-prwizard:
	@mkdir -p bin/
	@export GOOS=linux && export GOARCH=amd64 && go build -o "bin/prwizard-$(GIT_COMMIT).linux-amd64" -v cmd/prwizard/main.go

.PHONY: linux-ticketmaster
linux-ticketmaster:
	@mkdir -p bin/
	@export GOOS=linux && export GOARCH=amd64 && go build -o "bin/ticketmaster-$(GIT_COMMIT).linux-amd64" -v cmd/ticketmaster/main.go

.PHONY: linux-watcher
linux-watcher:
	@mkdir -p bin/
	@export GOOS=linux && export GOARCH=amd64 && go build -o "bin/watcher-$(GIT_COMMIT).linux-amd64" -v cmd/watcher/main.go

.PHONY: test
test:
	@go test ./... -cover

.PHONY: test-report
test-report:
	@go test ./... -coverprofile=coverage.txt && go tool cover -html=coverage.txt
